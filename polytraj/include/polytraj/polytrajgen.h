/**
 * @file polytrajgen.cpp
 * @brief Polynomial trajectory optimization
 * @author Parker Lusk <plusk@mit.edu>
 * @date 8 February 2021
 */

#pragma once

#include <iostream>
#include <chrono>
#include <fstream>
#include <memory>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <qpOASES.hpp>

using namespace Eigen;
using namespace std;

namespace acl {
namespace polytraj {

  struct PolyCoeff
  {
    // Given poly P = a_0 + a_1 t + a_2 t^2 + ...
    // coeff = [a_0, a_1, a_2, ...]
    std::vector<double> coeff;
    int poly_order;
  };

  struct PolySpline
  {
    std::vector<PolyCoeff> poly_coeff;
    std::vector<double> knot_time;
    int n_seg;
    bool is_valid;
  };

  struct PolySplineXYZ
  {
    bool is_valid;
    PolySpline spline_x, spline_y, spline_z;
    int n_seg;
    int poly_order;
    std::vector<double> knot_time;
  };

  /**
   *  Lower level routine 
   **/

  Eigen::VectorXd t_vec(int poly_order, double time, int n_diff);

  /*
  * scaling matrix D : p(t)=p'D t_vec(tau) where t1<=t<=t2 , 0<=tau<=1
  */
  Eigen::MatrixXd time_scaling_mat(double dt, int poly_order); // consider using sparse matrix

  /*
  *  integral t_vec(n,tau,3)*t_vec(n,tau,3)' from 0 to 1
  */
  Eigen::MatrixXd integral_jerk_squared(int poly_order);

  Eigen::MatrixXd integral_snap_squared(int poly_order);

  /**
   * @brief 
   * 
   * @param samll_mat M  
   * @return MatrixXd blckdiag(M,M,M)
   */
  Eigen::MatrixXd expand3(Eigen::MatrixXd samll_mat);
  Eigen::MatrixXd blkdiag(Eigen::MatrixXd A1, Eigen::MatrixXd A2, Eigen::MatrixXd A3);
  Eigen::MatrixXd row_stack3(Eigen::MatrixXd mat1);
  Eigen::MatrixXd row_stack3(Eigen::MatrixXd mat1, Eigen::MatrixXd mat2, Eigen::MatrixXd mat3);

  // find belonging time interval in time series
  int find_spline_interval(const vector<double>& knots, double eval_t);

  // append mat_sub to mat in the row 
  void row_append(MatrixXd & mat,MatrixXd mat_sub);

  Eigen::Matrix3d eval_spline(PolySplineXYZ spline_xyz, double t_eval);

  Affine3d get_affine_corridor_pose(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2); // one affine corridor

  /**
   *  High level path planner 
   * 
   **/

  // trajectory solving mode (referred to README.md)
  struct TrajGenOpts
  {   
    int objective_derivative; // jerk minimization = 3 , snap minimization = 4
    
    bool is_waypoint_soft;
    double w_d; // weight for deviation

    bool is_single_corridor;  
    bool is_multi_corridor;  

    int poly_order;
    // In case of parallel corridor = true the followings are required
    double safe_r;
    int N_safe_pnts;   

    bool verbose = true; // print the optimization matrices 

  };

  struct Constraint
  {
    MatrixXd A;
    MatrixXd b;
  };

  struct QP_form
  {
    MatrixXd Q;
    MatrixXd H;
    MatrixXd A;
    MatrixXd b;
    MatrixXd Aeq;
    MatrixXd beq;    
  };

  struct QP_form_xyz
  {
    QP_form x;
    QP_form y;
    QP_form z;
  };


  class PathPlanner
  {
  public:
    string log_output_file_name;

    PathPlanner();

    bool is_spline_valid() { return spline_xyz.is_valid; }

    // update spline and current path 
    void path_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q, const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt);
    void qp_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q, const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt, QP_form_xyz& decouple_qp);
    void qp_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q, const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt, QP_form& couple_qp);

    // evaluate at a time horizon 
    // void horizon_eval_spline(int N_eval_interval);

    // visualization_msgs::Marker get_safe_corridor_marker() { return safe_corridor_marker; }
    // visualization_msgs::MarkerArray get_safe_corridor_single_marker() { return safe_corridor_marker_single_array; }
    // visualization_msgs::Marker get_knots_marker();

    // evaluate at a time
    // Eigen::Vector3d point_eval_spline(double t_eval);
    // Twist vel_eval_spline(double t_eval);
    // Twist accel_eval_spline(double t_eval);
    // ros data retrieving from path update

    // sub routine
    PolySpline get_solution(VectorXd sol,int poly_order,int n_seg );        
    PolySplineXYZ get_solution_couple(VectorXd sol,int poly_order,int n_seg ); // sol = [px1 py1 pz1 | px2 py2 pz2 | ...]
    VectorXd solveqp(QP_form qp_prob,bool& is_ok);
    Constraint get_init_constraint_mat(double x0, double v0, double a0, TrajGenOpts opt); // up to 2nd order conditions
    Constraint get_final_constraint_mat(double xf, double vf, double af, TrajGenOpts opt);  // up to 2nd order conditions
    Constraint get_continuity_constraint_mat(double dt1,double dt2,TrajGenOpts opt); // up to 2nd order continuity
    Constraint get_continuity_constraint_mat3(double dt1,double dt2,TrajGenOpts opt); // coupled version
    
    
    /**
     * @brief Get the corridor constraint mat object
     * 
     * @param pnt1
     * @param pnt2
     * @param t_vec
     * @param option
     * @return Constraint A = 6 x (blck_size_seg) / b = 6 x 1
     */
    Constraint get_corridor_constraint_mat(Eigen::Vector3d p1, Eigen::Vector3d p2, Eigen::VectorXd t_vec, TrajGenOpts option); // refer the research note (find A,b for safe corridor)

    // void write_spline(string file_name);

    PolySplineXYZ spline_xyz; // the coefficient of this polynomials
  private:
    bool is_path_computed;
    bool is_this_verbose = true; 
  };

} // ns polytraj
} // ns acl
