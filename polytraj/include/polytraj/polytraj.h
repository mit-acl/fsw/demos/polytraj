/**
 * @file polytraj.h
 * @brief Polynomial trajectory optimization
 * @author Parker Lusk <plusk@mit.edu>
 * @date 31 January 2021
 */

#pragma once

#include <iostream>
#include <iomanip>
#include <memory>
#include <tuple>
#include <vector>

#include <Eigen/Dense>

#include "polytraj/polytrajgen.h"

namespace acl {
namespace polytraj {

  using Waypoints = std::vector<Eigen::Vector3d>;

  struct Trajectory
  {
    std::vector<Eigen::Vector3d> q; ///< waypoints (incl. q0, current position)
    std::vector<double> qt; ///< time at each waypoint (incl. t0 == 0)
    double tf = 0; ///< total trajectory duration
    bool valid = false; ///< was QP solved successfully

    TrajGenOpts opts;
    PolySplineXYZ spline_xyz;

    /**
     * @brief      Evaluate the trajectory at a given time. 
     *
     * @param[in]  t     Time at which to evaluate. Note that 0 <= t <= tf.
     *
     * @return     3x3 matrix with [p v a] as columns
     */
    Eigen::Matrix3d eval(double t) const;

    std::tuple<Eigen::Affine3d, Eigen::Vector3d> getSafeCorridor(int m) const;

    friend std::ostream& operator<<(std::ostream& os, const Trajectory& traj)
    {
      double l = 0;
      for (size_t i=1; i<traj.q.size(); i++) l += (traj.q[i] - traj.q[i-1]).norm();
      os << "<Trajectory: tf = " << traj.tf << ", " << traj.q.size() << " waypoints, ";
      os << "path length = " << std::fixed << std::setprecision(1) << l << " m>";
      return os;
    }
  };

  /**
   * @brief      Wrapper for QP-based polynomial trajectory optimizer
   */
  class TrajectoryGenerator
  {
  public:
    TrajectoryGenerator();
    ~TrajectoryGenerator() = default;

    void setTimeAllocationMethod_VelocityRamp(double vmax, double amax, double timefactor = 1.0);
    void setTimeAllocationMethod_Uniform(double tf);
    void setTimeAllocationMethod_Fabian(double vmax, double amax, double magic = 6.5);

    /**
     * @brief      Create a trajectory from user-specified waypoints.
     *
     * @param[in]  q     Waypoints defining straight-line paths
     * @param[in]  opts  Trajectory generation options
     *
     * @return     Trajectory object that can be sampled
     */
    Trajectory fromWaypoints(const Waypoints& q, const TrajGenOpts& opts);

    /**
     * @brief      Create a trajectory from user-specified waypoints.
     *             Uses default options.
     *
     * @param[in]  q     Waypoints defining straight-line paths
     *
     * @return     Trajectory object that can be sampled
     */
    Trajectory fromWaypoints(const Waypoints& q);

  private:
    TrajGenOpts default_opts_; ///< default trajectory options
    PathPlanner planner; ///< TrajGen class

    /// \brief Time allocation parameters
    enum TimeAllocation { VelocityRamp, Fabian, Uniform };
    TimeAllocation time_allocation_method_;
    double vmax_, amax_; ///< maximum vel / accel norms
    double tf_, magic_fabian_, timefactor_; ///< various parameters


    std::vector<double> estimateSegmentTimesVelocityRamp(
                      const Waypoints& q, double v_max, double a_max,
                      double time_factor) const;

    std::vector<double> estimateSegmentTimesNfabian(
                              const Waypoints& q, double v_max, double a_max,
                              double magic_fabian_constant) const;

    double computeTimeVelocityRamp(
                      const Eigen::Vector3d& start, const Eigen::Vector3d& goal,
                      double v_max, double a_max) const;
  };

} // ns polytraj
} // ns acl
