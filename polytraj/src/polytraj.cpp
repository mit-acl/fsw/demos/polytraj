/**
 * @file polytraj.cpp
 * @brief Polynomial trajectory optimization
 * @author Parker Lusk <plusk@mit.edu>
 * @date 31 January 2021
 */

#include <iostream>
#include <limits>
#include <numeric>

#include "polytraj/polytraj.h"

namespace acl {
namespace polytraj {

Eigen::Matrix3d Trajectory::eval(double t) const
{
  Eigen::Matrix3d X = Eigen::Matrix3d::Zero(); // [p v a] as cols

  if (!valid) {
    X.col(0) = q[0];
    return X;
  }

  X = eval_spline(spline_xyz, t);

  return X;
}

// ----------------------------------------------------------------------------

std::tuple<Eigen::Affine3d, Eigen::Vector3d> Trajectory::getSafeCorridor(int m) const
{
  Eigen::Affine3d pose = get_affine_corridor_pose(q[m], q[m+1]);
  const double l = (q[m+1] - q[m]).norm();
  Eigen::Vector3d side_lengths;
  side_lengths << 2 * (l/2 + opts.safe_r), 2 * opts.safe_r, 2 * opts.safe_r;
  return std::make_pair(pose, side_lengths);
}

// ============================================================================

TrajectoryGenerator::TrajectoryGenerator()
{
  default_opts_.poly_order = 6;
  default_opts_.objective_derivative = 3;
  default_opts_.is_waypoint_soft = true;
  default_opts_.is_single_corridor = true;
  default_opts_.is_multi_corridor = false;
  default_opts_.w_d = 1;
  default_opts_.N_safe_pnts = 2;
  default_opts_.safe_r = 0.1;
  default_opts_.verbose = false;

  // default to slow trajectories
  setTimeAllocationMethod_VelocityRamp(0.5, 0.5);
}

// ----------------------------------------------------------------------------

void TrajectoryGenerator::setTimeAllocationMethod_VelocityRamp(double vmax, double amax, double timefactor)
{
  time_allocation_method_ = TimeAllocation::VelocityRamp;
  vmax_ = vmax;
  amax_ = amax;
  timefactor_ = timefactor;
}

// ----------------------------------------------------------------------------

void TrajectoryGenerator::setTimeAllocationMethod_Uniform(double tf)
{
  time_allocation_method_ = TimeAllocation::Uniform;
  tf_ = tf;
}

// ----------------------------------------------------------------------------

void TrajectoryGenerator::setTimeAllocationMethod_Fabian(double vmax, double amax, double magic)
{
  time_allocation_method_ = TimeAllocation::Fabian;
  vmax_ = vmax;
  amax_ = amax;
  magic_fabian_ = magic;
}

// ----------------------------------------------------------------------------

Trajectory TrajectoryGenerator::fromWaypoints(const Waypoints& q, const TrajGenOpts& opts)
{
  std::vector<double> qt; // the time at each waypoint
  qt.reserve(q.size());

  if (time_allocation_method_ == TimeAllocation::Uniform) {
    Eigen::VectorXd knots(q.size());
    knots.setLinSpaced(q.size(), 0, tf_);
    for (size_t i=0; i<knots.size(); i++) {
      qt.push_back(knots[i]);
    }

  } else if (time_allocation_method_ == TimeAllocation::VelocityRamp) {
    std::vector<double> ts = estimateSegmentTimesVelocityRamp(q, vmax_, amax_, timefactor_);
    qt.resize(ts.size() + 1, 0.0); // note that qt[0] == 0
    std::partial_sum(ts.begin(), ts.end(), qt.begin() + 1); // start qt[1]

  } else if (time_allocation_method_ == TimeAllocation::Fabian) {
    std::vector<double> ts = estimateSegmentTimesNfabian(q, vmax_, amax_, magic_fabian_);
    qt.resize(ts.size() + 1, 0.0); // note that qt[0] == 0
    std::partial_sum(ts.begin(), ts.end(), qt.begin() + 1); // start qt[1]

  } else {
    std::cerr << "Invalid time allocation method!" << std::endl;
    return {};
  }
  
  // assume starting at rest. TODO: Make more flexible
  const Eigen::Vector3d v0 = Eigen::Vector3d::Zero();
  const Eigen::Vector3d a0 = Eigen::Vector3d::Zero();

  // generate the trajectory
  planner.path_gen(qt, q, v0, a0, opts);

  // create trajectory object
  Trajectory traj;
  traj.q = q;
  traj.qt = qt;
  traj.tf = qt.back();
  traj.spline_xyz = planner.spline_xyz;
  traj.valid = planner.is_spline_valid();
  traj.opts = opts;

  return traj;
}

// ----------------------------------------------------------------------------

Trajectory TrajectoryGenerator::fromWaypoints(const Waypoints& q)
{
  return fromWaypoints(q, default_opts_);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

std::vector<double> TrajectoryGenerator::estimateSegmentTimesVelocityRamp(
                  const Waypoints& q, double v_max, double a_max,
                  double time_factor) const
{
  static constexpr double kMinSegmentTime = 0.1;
  const int M = q.size() - 1; // num segments
  std::vector<double> ts(M, 0);
  for (size_t i=0; i<ts.size(); ++i) {
    const Eigen::Vector3d start = q[i];
    const Eigen::Vector3d end = q[i + 1];
    double t = computeTimeVelocityRamp(start, end, v_max, a_max);
    ts[i] = std::max(kMinSegmentTime, t);
  }
  return ts;
}

// ----------------------------------------------------------------------------

double TrajectoryGenerator::computeTimeVelocityRamp(
                  const Eigen::Vector3d& start, const Eigen::Vector3d& goal,
                  double v_max, double a_max) const
{
  const double distance = (start - goal).norm();
  // Time to accelerate or decelerate to or from maximum velocity:
  const double acc_time = v_max / a_max;
  // Distance covered during complete acceleration or decelerate:
  const double acc_distance = 0.5 * v_max * acc_time;
  // Compute total segment time:
  if (distance < 2.0 * acc_distance) {
    // Case 1: Distance too small to accelerate to maximum velocity.
    return 2.0 * std::sqrt(distance / a_max);
  } else {
    // Case 2: Distance long enough to accelerate to maximum velocity.
    return 2.0 * acc_time + (distance - 2.0 * acc_distance) / v_max;
  }
}

// ----------------------------------------------------------------------------

std::vector<double> TrajectoryGenerator::estimateSegmentTimesNfabian(
                          const Waypoints& q, double v_max, double a_max,
                          double magic_fabian_constant) const
{
  const int M = q.size() - 1; // num segments
  std::vector<double> ts(M, 0);
  for (size_t i=0; i<ts.size(); ++i) {
    const Eigen::Vector3d start = q[i];
    const Eigen::Vector3d end = q[i + 1];
    const double distance = (end - start).norm();
    ts[i] = distance / v_max * 2 *
               (1.0 + magic_fabian_constant * v_max / a_max *
                          std::exp(-distance / v_max * 2));
  }
  return ts;
}

} // ns polytraj
} // ns acl
