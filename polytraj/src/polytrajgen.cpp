/**
 * @file polytrajgen.cpp
 * @brief Polynomial trajectory optimization
 * @author Parker Lusk <plusk@mit.edu>
 * @date 8 February 2021
 */

#include "polytraj/polytrajgen.h"

namespace acl {
namespace polytraj {

PathPlanner::PathPlanner()
: is_path_computed(false)
{}

// ----------------------------------------------------------------------------

Eigen::Affine3d get_affine_corridor_pose(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2)
{
  // Build a rotation matrix of bbox basis w.r.t world

  // x-axis of bbox is along line segment btwn waypoints, pointing at p2
  const Eigen::Vector3d e1 = (p2 - p1).normalized();

  // y-axis
  Eigen::Vector3d e2 = Eigen::Vector3d::UnitY();
  if (e1.x() != 0 && e1.y() != 0) {
    e2.x() = - e1.y() / e1.x() * e2.y();
  } else if (e1.x() == 0 && e1.y() != 0) {
    e2 = Eigen::Vector3d::UnitX();
  } else if (e1.x() != 0 && e1.y() == 0) {
    e2 = Eigen::Vector3d::UnitY();
  } else {
    e2 = Eigen::Vector3d(1, 1, 0);
  }
  e2.normalize();

  // z-axis
  const Eigen::Vector3d e3 = e1.cross(e2);

  Eigen::Matrix3d Rwb;
  Rwb.col(0) = e1;
  Rwb.col(1) = e2;
  Rwb.col(2) = e3;

  Eigen::Affine3d Twb;
  Twb.linear() = Rwb;
  Twb.translation() = (p1 + p2) / 2.;
  return Twb; 
}

// ----------------------------------------------------------------------------

// path generation core rountine 
void PathPlanner::path_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q,
  const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt)
{
  is_this_verbose = opt.verbose;
  const int n_seg = q.size() - 1;
  const int poly_order = opt.poly_order;

  PolySpline spline_x;
  PolySpline spline_y;
  PolySpline spline_z;
  bool is_ok = false;

  if (opt.is_single_corridor) { // coupled
    // std::cout << "[TRAJ_GEN] xyz coupled corridor generation..." << std::endl;
    QP_form qp;
    qp_gen(qt, q, v0, a0, opt, qp);
    PolySplineXYZ spline_xyz_sol = get_solution_couple(solveqp(qp, is_ok), poly_order, n_seg);
    spline_x = spline_xyz_sol.spline_x;
    spline_y = spline_xyz_sol.spline_y;
    spline_z = spline_xyz_sol.spline_z;
  } else { // decoupled
    // std::cout << "[TRAJ_GEN] xyz decoupled corridor generation..." << std::endl;
    QP_form_xyz qp_xyz;
    qp_gen(qt, q, v0, a0, opt, qp_xyz);
    bool is_ok_x, is_ok_y, is_ok_z;
    spline_x = get_solution(solveqp(qp_xyz.x, is_ok_x), poly_order, n_seg);
    spline_y = get_solution(solveqp(qp_xyz.y, is_ok_y), poly_order, n_seg);
    spline_z = get_solution(solveqp(qp_xyz.z, is_ok_z), poly_order, n_seg);
    is_ok = is_ok_x && is_ok_y && is_ok_z;
  }

  // Rescale the solution
  for (int k=0; k<n_seg; k++){
      for (int n=0; n<=poly_order; n++){
          spline_x.poly_coeff[k].coeff[n] /= pow(qt[k+1]-qt[k],n);
          spline_y.poly_coeff[k].coeff[n] /= pow(qt[k+1]-qt[k],n);
          spline_z.poly_coeff[k].coeff[n] /= pow(qt[k+1]-qt[k],n);
      }
  }

  is_path_computed = true;

  // finalizing the path 
  spline_xyz.spline_x = spline_x;
  spline_xyz.spline_y = spline_y;
  spline_xyz.spline_z = spline_z;
  spline_xyz.is_valid = is_ok;
  spline_xyz.knot_time = qt;
  spline_xyz.n_seg = spline_x.n_seg;
  spline_xyz.poly_order = poly_order;
}

// ----------------------------------------------------------------------------

/**
 * @brief This is qp generator for coupled case (single corridor)
 * 
 * @param knots 
 * @param waypoints 
 * @param v0 
 * @param a0 
 * @param opt 
 * @param qp_form 
 */
void PathPlanner::qp_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q,
  const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt, QP_form& qp_form)
{
  const int n_seg = q.size() - 1;
  const int poly_order = opt.poly_order;

  // this should be changed when we adopt single box 
  const int n_var_total = 3 * (poly_order + 1) * n_seg;  // xyz
  const int blck_size = poly_order+1;
  const int blck_size_seg = 3*blck_size;  // stride along one segment of path 

  Eigen::MatrixXd Q = Eigen::MatrixXd::Zero(n_var_total, n_var_total);
  Eigen::MatrixXd H = Eigen::MatrixXd::Zero(1, n_var_total);

  Eigen::MatrixXd Aeq = Eigen::MatrixXd::Zero(0, n_var_total);
  Eigen::MatrixXd beq = Eigen::MatrixXd::Zero(0, 1);
  Eigen::MatrixXd Aineq = Eigen::MatrixXd::Zero(0, n_var_total);
  Eigen::MatrixXd bineq = Eigen::MatrixXd::Zero(0, 1);

  //
  // 1. Build the Cost Function
  //

  if (opt.objective_derivative == 3) { // min jerk
    for (int n=0; n<n_seg; n++) {
      // Eigen::MatrixXd Dn = time_scaling_mat(qt[n + 1]-qt[n], poly_order);
      // double dn = qt[n + 1] - qt[n];
      // Q.block(blck_size*(n),blck_size*(n),blck_size,blck_size)=Dn*integral_jerk_squared(poly_order)*Dn/pow(dn,5);
      Q.block(blck_size_seg*n,blck_size_seg*n,blck_size_seg,blck_size_seg) = expand3(integral_jerk_squared(poly_order));
    }
  } else if (opt.objective_derivative == 4) { // min snap
    for (int n=0; n<n_seg; n++) {
      // Eigen::MatrixXd Dn = time_scaling_mat(qt[n + 1]-qt[n], poly_order);
      // double dn = qt[n + 1] - qt[n];
      // Q.block(blck_size*(n),blck_size*(n),blck_size,blck_size)=Dn*integral_snap_squared(poly_order)*Dn/pow(dn,7);
      Q.block(blck_size_seg*n,blck_size_seg*n,blck_size_seg,blck_size_seg) = expand3(integral_snap_squared(poly_order));
    }
  } else {
    std::cerr << "undefined derivative in objective" << std::endl;
  }

  // if it is soft, we include the deviation term 
  if (opt.is_waypoint_soft) {
    for (int n=0; n<n_seg; n++) {
      // Eigen::MatrixXd Dn = time_scaling_mat(qt[n + 1]-qt[n], poly_order);
      double time_scaling_factor;
      if (opt.objective_derivative == 3) {
        time_scaling_factor = pow(qt[n+1]-qt[n],5);
      } else {
        time_scaling_factor = pow(qt[n+1]-qt[n],7);
      }

      // add soft constraint on waypoint deviation to cost
      const Eigen::MatrixXd wpdcost = time_scaling_factor * opt.w_d * t_vec(poly_order,1,0) * t_vec(poly_order,1,0).transpose();
      Q.block(blck_size_seg*n,blck_size_seg*n,blck_size_seg,blck_size_seg) += expand3(wpdcost);

      // what is H? derivative of cost?
      Eigen::MatrixXd H_sub(1,blck_size_seg);
      H_sub << -2 * time_scaling_factor * opt.w_d * (q[n+1].x()) * t_vec(poly_order,1,0).transpose(),
               -2 * time_scaling_factor * opt.w_d * (q[n+1].y()) * t_vec(poly_order,1,0).transpose(),
               -2 * time_scaling_factor * opt.w_d * (q[n+1].z()) * t_vec(poly_order,1,0).transpose();
      H.block(0,blck_size_seg*n,1,blck_size_seg) = H_sub;
    }
  }

  //
  // 2. Formulate Equality Constraints
  //    

  // (1) Initial constraints 
  Eigen::MatrixXd Aeq0 = Eigen::MatrixXd::Zero(9,n_var_total);
  Eigen::MatrixXd beq0_sub = Eigen::MatrixXd::Zero(9,1);
  Aeq0.block(0,0,9,blck_size_seg) = blkdiag(get_init_constraint_mat(q[0].x(),v0.x(),a0.x(),opt).A,
                                            get_init_constraint_mat(q[0].y(),v0.y(),a0.y(),opt).A,
                                            get_init_constraint_mat(q[0].z(),v0.z(),a0.z(),opt).A);
  row_append(Aeq, Aeq0);

  beq0_sub  = row_stack3(get_init_constraint_mat(q[0].x(),v0.x(),a0.x(),opt).b,
                         get_init_constraint_mat(q[0].y(),v0.x(),a0.y(),opt).b,
                         get_init_constraint_mat(q[0].z(),v0.x(),a0.z(),opt).b);

  row_append(beq, beq0_sub);

  // (2) Waypoints constraints (if it is hard constrained)
  if (!opt.is_waypoint_soft) {
    for (int n=0; n<n_seg; n++) {
      // Eigen::MatrixXd Dn = time_scaling_mat(qt[n + 1]-qt[n], poly_order);
      Eigen::MatrixXd Aeq_sub = Eigen::MatrixXd::Zero(3,n_var_total);
      Eigen::MatrixXd beq_sub = Eigen::MatrixXd(3,1);
      Aeq_sub.block(0,blck_size_seg*n,1,blck_size_seg) = expand3(t_vec(poly_order,1,0).transpose());
      row_append(Aeq, Aeq_sub); 

      beq_sub = q[n+1];
      row_append(beq, beq_sub);
    }
  }

  // (3) continuity constraints     
  // if the objective is snap, we will include 3rd order continuity (?)
  for(int n = 0; n<n_seg-1; n++) {
    // TODO: should one of the 3's be replaced with opt.objective_derivative? (see other qp_gen fcn)
    Eigen::MatrixXd Aeq_sub = Eigen::MatrixXd::Zero(3*3,n_var_total);
    Eigen::MatrixXd beq_sub = Eigen::MatrixXd::Zero(3*3,1);
    Aeq_sub.block(0,blck_size_seg*n,3*3,2*blck_size_seg) = get_continuity_constraint_mat3(qt[n+1]-qt[n], qt[n+2]-qt[n+1], opt).A;
    beq_sub = get_continuity_constraint_mat3(qt[n+1]-qt[n], qt[n+2]-qt[n+1], opt).b;
    row_append(Aeq, Aeq_sub);
    row_append(beq, beq_sub);
  }

  // (4) final constraints
  Eigen::MatrixXd Aeqn = Eigen::MatrixXd::Zero(9,n_var_total);
  Eigen::MatrixXd beqn_sub = Eigen::MatrixXd::Zero(9,1);
  Aeqn.block(0,blck_size_seg*(n_seg-1),9,blck_size_seg) = blkdiag(get_final_constraint_mat(q.back().x(),v0.x(),a0.x(),opt).A,
                                            get_final_constraint_mat(q.back().y(),v0.y(),a0.y(),opt).A,
                                            get_final_constraint_mat(q.back().z(),v0.z(),a0.z(),opt).A);
  row_append(Aeq, Aeqn);

  beqn_sub  = row_stack3(get_final_constraint_mat(q.back().x(),v0.x(),a0.x(),opt).b,
                         get_final_constraint_mat(q.back().y(),v0.x(),a0.y(),opt).b,
                         get_final_constraint_mat(q.back().z(),v0.x(),a0.z(),opt).b);

  row_append(beq, beqn_sub);

  //
  // 3. Formulate Inequality Constraints (for corridor constraint)
  //

  const int N_safe_pnts = opt.N_safe_pnts + 2;
  const int n_ineq_consts = 3*2*(N_safe_pnts)*n_seg;

  Eigen::MatrixXd A_sub = Eigen::MatrixXd::Zero(n_ineq_consts, n_var_total);
  Eigen::MatrixXd b_sub = Eigen::MatrixXd::Zero(n_ineq_consts, 1);

  int ineq_row_insert_idx = 0;
  int ineq_col_insert_idx = 0;

  // per segment 
  for (int n = 0; n<n_seg ;n++) {
      // per safe sample points along 
      for (int n_sub=0; n_sub<N_safe_pnts; n_sub++) {
          double t_control = 1.0/(N_safe_pnts-1) * n_sub;
          
          // get A,b block for this control point 
          Constraint const_ineq = get_corridor_constraint_mat(q[n], q[n+1], t_vec(poly_order,t_control,0), opt);

          A_sub.block(ineq_row_insert_idx,ineq_col_insert_idx,6,blck_size_seg) = const_ineq.A;
          b_sub.block(ineq_row_insert_idx,0,6,1) = const_ineq.b;

          // stride along row
          ineq_row_insert_idx += 6;
      }

      // stride along col
      ineq_col_insert_idx += blck_size_seg;
  }
  row_append(Aineq, A_sub);
  row_append(bineq, b_sub);

  qp_form.Q = Q;
  qp_form.H = H;
  qp_form.A = Aineq;
  qp_form.b = bineq;
  qp_form.Aeq = Aeq;
  qp_form.beq = beq;
}

/**
 * @brief decoupled qp generation
 *
 * @param knots
 * @param waypoints
 * @param v0
 * @param a0
 * @param opt
 * @param qp_form_xyz
 */
void PathPlanner::qp_gen(const std::vector<double>& qt, const std::vector<Eigen::Vector3d>& q,
  const Eigen::Vector3d& v0, const Eigen::Vector3d& a0, TrajGenOpts opt, QP_form_xyz& qp_form_xyz)
{
  const int n_seg = q.size() - 1;
  const int poly_order = opt.poly_order;

  // this should be changed when we adopt single box
  const int n_var_total = (poly_order + 1) * n_seg; // separable x, y, z optim
  const int blck_size = poly_order+1;

  Eigen::MatrixXd Q = Eigen::MatrixXd::Zero(n_var_total, n_var_total);
  Eigen::MatrixXd H = Eigen::MatrixXd::Zero(1, n_var_total);

  Eigen::MatrixXd Aeq = Eigen::MatrixXd::Zero(0, n_var_total);
  Eigen::MatrixXd beq = Eigen::MatrixXd::Zero(0, 1);
  Eigen::MatrixXd Aineq = Eigen::MatrixXd::Zero(0, n_var_total);
  Eigen::MatrixXd bineq = Eigen::MatrixXd::Zero(0, 1);

  // since this problem is decoupled...
  MatrixXd Hx = H, Hy = H, Hz = H;
  MatrixXd Aeq_x = Aeq, Aeq_y = Aeq, Aeq_z = Aeq;
  MatrixXd beq_x = beq, beq_y = beq, beq_z = beq;
  MatrixXd Aineq_x = Aineq, Aineq_y = Aineq, Aineq_z = Aineq;
  MatrixXd bineq_x = bineq, bineq_y = bineq, bineq_z = bineq;
    

  //
  // 1. Build the Cost Function
  //

  if (opt.objective_derivative == 3) { // min jerk
    for (int n=0; n<n_seg; n++) {
      Q.block(blck_size*n,blck_size*n,blck_size,blck_size) = integral_jerk_squared(poly_order);
    }
  } else if(opt.objective_derivative == 4) { // min snap
    for (int n=0; n<n_seg; n++) {
      Q.block(blck_size*n,blck_size*n,blck_size,blck_size) = integral_snap_squared(poly_order);
    }
  } else {
    std::cerr << "undefined derivative in objective" << std::endl;
  }

  // if it is soft, we include the deviation term
  if (opt.is_waypoint_soft) {
    for (int n=0; n<n_seg; n++) {
      double time_scaling_factor;
      if (opt.objective_derivative == 3) {
        time_scaling_factor = std::pow(qt[n+1]-qt[n],5);
      } else {
        time_scaling_factor = std::pow(qt[n+1]-qt[n],7);
      }

      // add soft constraint on waypoint deviation to cost
      const Eigen::MatrixXd wpdcost = time_scaling_factor * opt.w_d * t_vec(poly_order,1,0) * t_vec(poly_order,1,0).transpose();
      Q.block(blck_size*n,blck_size*n,blck_size,blck_size) += wpdcost;

      // what is H? derivative of cost?
      Hx.block(0,blck_size*n,1,blck_size) = -2 * time_scaling_factor * opt.w_d * q[n+1].x() * t_vec(poly_order,1,0).transpose();
      Hy.block(0,blck_size*n,1,blck_size) = -2 * time_scaling_factor * opt.w_d * q[n+1].y() * t_vec(poly_order,1,0).transpose();
      Hz.block(0,blck_size*n,1,blck_size) = -2 * time_scaling_factor * opt.w_d * q[n+1].z() * t_vec(poly_order,1,0).transpose();
    }
  }

  //
  // 2. Formulate Equality Constraints
  //

  // (1) Initial constraints
  Eigen::MatrixXd Aeq0 = Eigen::MatrixXd::Zero(3,n_var_total);
  Aeq0.block(0,0,3,blck_size) = get_init_constraint_mat(q[0].x(),v0.x(),a0.x(),opt).A;
  row_append(Aeq_x, Aeq0);
  Aeq0.block(0,0,3,blck_size) = get_init_constraint_mat(q[0].y(),v0.y(),a0.y(),opt).A;
  row_append(Aeq_y, Aeq0);
  Aeq0.block(0,0,3,blck_size) = get_init_constraint_mat(q[0].z(),v0.z(),a0.z(),opt).A;
  row_append(Aeq_z, Aeq0);

  Eigen::MatrixXd beq0_sub = Eigen::MatrixXd::Zero(3,1);
  beq0_sub.block(0,0,3,1) = get_init_constraint_mat(q[0].x(),v0.x(),a0.x(),opt).b;
  row_append(beq_x, beq0_sub);
  beq0_sub.block(0,0,3,1) = get_init_constraint_mat(q[0].y(),v0.y(),a0.y(),opt).b;
  row_append(beq_y, beq0_sub);
  beq0_sub.block(0,0,3,1) = get_init_constraint_mat(q[0].z(),v0.z(),a0.z(),opt).b;
  row_append(beq_z, beq0_sub);

  // (2) Waypoints constraints (if it is hard constrained)
  if (!opt.is_waypoint_soft) {
    for(int n=0; n<n_seg; n++) {
      Eigen::MatrixXd Aeq_sub = Eigen::MatrixXd::Zero(1,n_var_total);
      Eigen::MatrixXd beq_sub = Eigen::MatrixXd(1,1);
      Aeq_sub.block(0,blck_size*n,1,blck_size) = t_vec(poly_order,1,0).transpose();
      row_append(Aeq_x, Aeq_sub);
      row_append(Aeq_y, Aeq_sub);
      row_append(Aeq_z, Aeq_sub);

      beq_sub(0) = q[n+1].x();
      row_append(beq_x, beq_sub);
      beq_sub(0) = q[n+1].y();
      row_append(beq_y, beq_sub);
      beq_sub(0) = q[n+1].z();
      row_append(beq_z, beq_sub);
    }
  }

  // (3) continuity constraints
  // if the objective is snap, we will include 3rd order continuity (?)
  for (int n=0; n<n_seg-1; n++) {
    Eigen::MatrixXd Aeq_sub = Eigen::MatrixXd::Zero(opt.objective_derivative,n_var_total);
    Aeq_sub.block(0,blck_size*n,opt.objective_derivative,2*blck_size) = get_continuity_constraint_mat(qt[n+1]-qt[n],qt[n+2]-qt[n+1],opt).A;
    row_append(Aeq_x, Aeq_sub);
    row_append(Aeq_y, Aeq_sub);
    row_append(Aeq_z, Aeq_sub);

    Eigen::MatrixXd beq_sub = Eigen::MatrixXd::Zero(opt.objective_derivative,1);
    beq_sub = get_continuity_constraint_mat(qt[n+1]-qt[n],qt[n+2]-qt[n+1],opt).b;
    row_append(beq_x, beq_sub);
    row_append(beq_y, beq_sub);
    row_append(beq_z, beq_sub);
  }

  // TODO: where are final constraints?

  //
  // 3. Formulate Inequality Constraints (for corridor constraint)
  //

  if (opt.is_multi_corridor) {

    const double safe_r = opt.safe_r;
    const int N_safe_pnts = opt.N_safe_pnts;
    const int n_ineq_consts = 2 * N_safe_pnts * n_seg;

    Eigen::MatrixXd A_sub = Eigen::MatrixXd::Zero(n_ineq_consts, n_var_total);
    Eigen::MatrixXd bx_sub = Eigen::MatrixXd::Zero(n_ineq_consts, 1);
    Eigen::MatrixXd by_sub = Eigen::MatrixXd::Zero(n_ineq_consts, 1);
    Eigen::MatrixXd bz_sub = Eigen::MatrixXd::Zero(n_ineq_consts, 1);
        
    int ineq_col_insert_idx1 = 0;
    int ineq_row_insert_idx = 0;

    // per segment
    for (int n=0; n<n_seg; n++) {

      const Eigen::Vector3d q0 = q[n];
      const Eigen::Vector3d qf = q[n+1];
      const Eigen::Vector3d delta = (qf - q0) / (N_safe_pnts + 1);

      // per safe sample points along
      for (int n_sub=1; n_sub<=N_safe_pnts; n_sub++) {

        const Eigen::Vector3d qs = q0 + delta * n_sub;
        const double t_control = (1. / (N_safe_pnts + 1)) * n_sub;

        // axis parallel multiple cube
        const Eigen::Vector3d lb = qs - safe_r * Eigen::Vector3d::Ones();
        const Eigen::Vector3d ub = qs + safe_r * Eigen::Vector3d::Ones();

        // non parallel single rectangle
        A_sub.block(ineq_row_insert_idx,ineq_col_insert_idx1,1,blck_size) = -t_vec(poly_order,t_control,0).transpose();

        // lower limit
        bx_sub.coeffRef(ineq_row_insert_idx) = -lb.x();
        by_sub.coeffRef(ineq_row_insert_idx) = -lb.y();
        bz_sub.coeffRef(ineq_row_insert_idx) = -lb.z();

        ineq_row_insert_idx++;

        // upper limit
        A_sub.block(ineq_row_insert_idx,ineq_col_insert_idx1,1,blck_size) = t_vec(poly_order,t_control,0).transpose();
        bx_sub.coeffRef(ineq_row_insert_idx) = ub.x();
        by_sub.coeffRef(ineq_row_insert_idx) = ub.y();
        bz_sub.coeffRef(ineq_row_insert_idx) = ub.z();

        ineq_row_insert_idx++;

      }
      ineq_col_insert_idx1 += blck_size;
    }

    row_append(Aineq_x, A_sub);
    row_append(Aineq_y, A_sub);
    row_append(Aineq_z, A_sub);

    row_append(bineq_x, bx_sub);
    row_append(bineq_y, by_sub);
    row_append(bineq_z, bz_sub);
  }
    
  //
  // 4. Pack-up variables
  //

  QP_form qp_x; qp_x.Q = Q; qp_x.H = Hx; qp_x.A = Aineq_x; qp_x.b = bineq_x; qp_x.Aeq = Aeq_x; qp_x.beq = beq_x;
  QP_form qp_y; qp_y.Q = Q; qp_y.H = Hy; qp_y.A = Aineq_y; qp_y.b = bineq_y; qp_y.Aeq = Aeq_y; qp_y.beq = beq_y;
  QP_form qp_z; qp_z.Q = Q; qp_z.H = Hz; qp_z.A = Aineq_z; qp_z.b = bineq_z; qp_z.Aeq = Aeq_z; qp_z.beq = beq_z;

  qp_form_xyz.x = qp_x;
  qp_form_xyz.y = qp_y;
  qp_form_xyz.z = qp_z;
}

// ----------------------------------------------------------------------------

Eigen::Matrix3d eval_spline(PolySplineXYZ spline_xyz, double t_eval)
{
  const int poly_order = spline_xyz.poly_order;
  t_eval = std::min(spline_xyz.knot_time.back(), t_eval);
  t_eval = std::max(spline_xyz.knot_time.front(), t_eval);

  Eigen::Index spline_idx = find_spline_interval(spline_xyz.knot_time, t_eval);
  const double tau = t_eval - spline_xyz.knot_time[spline_idx];

  Eigen::Matrix3d X;
  for (size_t i=0; i<3; i++) {
    X.col(i).x() = t_vec(poly_order,tau,i).transpose() * Map<VectorXd>(spline_xyz.spline_x.poly_coeff[spline_idx].coeff.data(),poly_order+1);
    X.col(i).y() = t_vec(poly_order,tau,i).transpose() * Map<VectorXd>(spline_xyz.spline_y.poly_coeff[spline_idx].coeff.data(),poly_order+1);
    X.col(i).z() = t_vec(poly_order,tau,i).transpose() * Map<VectorXd>(spline_xyz.spline_z.poly_coeff[spline_idx].coeff.data(),poly_order+1);
  }
  return X;
}

// ----------------------------------------------------------------------------

Eigen::VectorXd PathPlanner::solveqp(QP_form qp_prob, bool& is_ok)
{
  is_ok = true;
  Eigen::MatrixXd Q = qp_prob.Q;
  Eigen::MatrixXd H = qp_prob.H;
  Eigen::MatrixXd Aineq = qp_prob.A;
  Eigen::MatrixXd bineq = qp_prob.b;
  Eigen::MatrixXd Aeq = qp_prob.Aeq;
  Eigen::MatrixXd beq = qp_prob.beq;
  if (is_this_verbose) {
      std::cout << "Q: " << std::endl;
      std::cout << Q << std::endl;
      
      std::cout << "H: " << std::endl;
      std::cout << H << std::endl;
          
      std::cout << "Aineq: " << std::endl;
      std::cout << Aineq << std::endl;

      std::cout << "bineq: " << std::endl;
      std::cout << bineq << std::endl;

      std::cout << "Aeq: " << std::endl;
      std::cout << Aeq << std::endl;

      std::cout << "beq: " << std::endl;
      std::cout << beq << std::endl;
  }
  USING_NAMESPACE_QPOASES;

  const int N_var = Q.rows();
  const int N_ineq_const = Aineq.rows();
  const int N_eq_const = Aeq.rows();
  const int N_const = N_ineq_const + N_eq_const;

  real_t H_qp[N_var*N_var];
  real_t g[N_var];
  real_t A[N_var*N_const];
      real_t lbA[N_const];
  real_t ubA[N_const];

  // cost
  for (int i = 0;i<N_var;i++){
      g[i] = H(0,i);            
      for(int j = 0;j<N_var;j++)
          H_qp[j*N_var+i] = 2*Q(i,j);
  }
  
  // eq constraints
  for (int r = 0; r<N_eq_const;r++){
      lbA[r] = beq(r);
      ubA[r] = beq(r);
      for(int c= 0; c<N_var;c++)
          A[r*N_var + c] = Aeq(r,c);
  }

  // ineq constraints
  for (int r = 0; r<N_ineq_const;r++){
      lbA[N_eq_const+r] = -1e+8;
      ubA[N_eq_const+r] = bineq(r);
      for(int c= 0; c<N_var;c++)
          A[(r+N_eq_const)*N_var + c] = Aineq(r,c);
  }    

  int_t nWSR = 2000;
 
  QProblem qp_obj(N_var,N_const,HST_SEMIDEF);
  //std::cout<<"hessian type: "<<qp_obj.getHessianType()<<endl;
  Options options;
  options.printLevel = PL_LOW;
  qp_obj.setOptions(options);
  qp_obj.init(H_qp, g, A, NULL, NULL, lbA, ubA, nWSR);
  if (qp_obj.isInfeasible()) {
      std::cout << "[QP solver] warning: problem is infeasible." << std::endl;
      is_ok = false;
  }
  real_t xOpt[N_var];
  qp_obj.getPrimalSolution(xOpt);

  if (!qp_obj.isSolved()) {
      std::cout << "[QP solver] quadratic programming has not been solved" << std::endl;
      is_ok = false;
  }
  
  Eigen::VectorXd sol(N_var);

  for(int n = 0; n<N_var;n++)
      sol(n) = xOpt[n];

  //std::cout << "solution" << std::endl;
  //std::cout << sol << std::endl;
  return sol;
}

// ----------------------------------------------------------------------------

PolySpline PathPlanner::get_solution(Eigen::VectorXd sol, int poly_order, int n_seg)
{
  // this initialization of spline object 
  PolySpline polySpline;
  
  polySpline.n_seg = n_seg;
  // polySpline.knot_time.reserve(n_seg+1);  // let's insert the time later outside of this fucntion 
  polySpline.poly_coeff.reserve(n_seg);  // let's insert the time later outside of this fucntion 
      
  for(uint i = 0; i<polySpline.n_seg;i++)
      polySpline.poly_coeff[i].poly_order=poly_order;
  int n_var=n_seg*(poly_order+1);

  // std::cout<<"[DEBUG] solution:"<<std::endl;
  // std::cout<<var<<std::endl;
  // from lowest order 0
  for(int n=0;n<n_seg;n++){
      PolyCoeff coeff;
      coeff.coeff.resize(poly_order+1);
      for(int i=0;i<poly_order+1;i++){
          coeff.coeff[i]=sol(n*(poly_order+1)+i); 
          coeff.poly_order = poly_order;         
      }
      polySpline.poly_coeff.push_back(coeff);
  }

  return polySpline;
}

// ----------------------------------------------------------------------------

PolySplineXYZ PathPlanner::get_solution_couple(Eigen::VectorXd sol, int poly_order, int n_seg)
{
  int D = 3; // xyz
  std::vector<PolySpline> polySplineXYZ(D);
  int blck_size = poly_order + 1;
  int blck_size_seg = blck_size *3;

  for(int d = 0; d<D ; d++){
      polySplineXYZ[d].n_seg = n_seg; polySplineXYZ[d].poly_coeff.reserve(n_seg);

      for(uint i = 0; i<polySplineXYZ[d].n_seg;i++)
          polySplineXYZ[d].poly_coeff[i].poly_order=poly_order;
      int n_var=n_seg*(poly_order+1);

      // std::cout<<"[DEBUG] solution:"<<std::endl;
      // std::cout<<var<<std::endl;
      // from lowest order 0
      for(int n=0;n<n_seg;n++){
          PolyCoeff coeff;
          coeff.coeff.resize(poly_order+1);
          for(int i=0;i<blck_size;i++){
              coeff.coeff[i]=sol(n*blck_size_seg+d*blck_size+i); 
              coeff.poly_order = poly_order;         
          }
          polySplineXYZ[d].poly_coeff.push_back(coeff);
      }
  }

  PolySplineXYZ spline_xyz_temp;
  spline_xyz_temp.spline_x = polySplineXYZ[0];
  spline_xyz_temp.spline_y = polySplineXYZ[1];
  spline_xyz_temp.spline_z = polySplineXYZ[2];

  return spline_xyz_temp;
}

// ----------------------------------------------------------------------------
       
// output size 3x(N+1)
Constraint PathPlanner::get_init_constraint_mat(double x0, double v0, double a0, TrajGenOpts opt)
{
  int poly_order = opt.poly_order;
  Eigen::MatrixXd Aeq0 = Eigen::MatrixXd::Zero(3,poly_order+1);
  Eigen::MatrixXd beq0 = Eigen::MatrixXd::Zero(3,1);
  double dt_arbitrary = 1;

  // Aeq0.row(0) = t_vec(poly_order,0,0).transpose()*time_scaling_mat(dt_arbitrary,poly_order);
  // Aeq0.row(1) = t_vec(poly_order,0,1).transpose()*time_scaling_mat(dt_arbitrary,poly_order)/dt_arbitrary;
  // Aeq0.row(2) = t_vec(poly_order,0,2).transpose()*time_scaling_mat(dt_arbitrary,poly_order)/pow(dt_arbitrary,2);

  Aeq0.row(0) = t_vec(poly_order,0,0).transpose();
  Aeq0.row(1) = t_vec(poly_order,0,1).transpose()/dt_arbitrary;
  Aeq0.row(2) = t_vec(poly_order,0,2).transpose()/pow(dt_arbitrary,2);

  beq0(0) = x0;
  beq0(1) = v0;
  beq0(2) = a0;
  
  Constraint constraint;
  constraint.A = Aeq0;
  constraint.b = beq0;
  return constraint;
}

// ----------------------------------------------------------------------------
       
// output size 3x(N+1)
Constraint PathPlanner::get_final_constraint_mat(double xf, double vf, double af, TrajGenOpts opt)
{
  int poly_order = opt.poly_order;
  Eigen::MatrixXd Aeqf = Eigen::MatrixXd::Zero(3,poly_order+1);
  Eigen::MatrixXd beqf = Eigen::MatrixXd::Zero(3,1);

  Aeqf.row(0) = t_vec(poly_order,1,0).transpose();
  Aeqf.row(1) = t_vec(poly_order,1,1).transpose();
  Aeqf.row(2) = t_vec(poly_order,1,2).transpose();

  beqf(0) = xf;
  beqf(1) = vf;
  beqf(2) = af;
  
  Constraint constraint;
  constraint.A = Aeqf;
  constraint.b = beqf;
  return constraint;
}

// ----------------------------------------------------------------------------

// output size 3x(2(N+1))
Constraint PathPlanner::get_continuity_constraint_mat(double dt1, double dt2, TrajGenOpts opt)
{
  int N_constraint = opt.objective_derivative;
  int poly_order = opt.poly_order;
  Eigen::MatrixXd Aeq(N_constraint,2*(poly_order+1));
  Eigen::MatrixXd beq(N_constraint,1); beq.setZero();   
  Aeq.setZero();
  beq.setZero();
  Eigen::MatrixXd D1 = time_scaling_mat(dt1,poly_order);
  Eigen::MatrixXd D2 = time_scaling_mat(dt2,poly_order);

  // 0th order         
  // Aeq. block(0,0,1,poly_order+1) = t_vec(poly_order,1,0).transpose()*D1;
  // Aeq.block(0,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,0).transpose()*D2;

  Aeq.block(0,0,1,poly_order+1) = t_vec(poly_order,1,0).transpose();
  Aeq.block(0,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,0).transpose();


  // 1st order
  // Aeq.block(1,0,1,poly_order+1) = t_vec(poly_order,1,1).transpose()*D1*dt2;
  // Aeq.block(1,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,1).transpose()*D2*dt1; 

  Aeq.block(1,0,1,poly_order+1) = t_vec(poly_order,1,1).transpose()*dt2;
  Aeq.block(1,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,1).transpose()*dt1; 


  // 2nd order
  Aeq.block(2,0,1,poly_order+1) = t_vec(poly_order,1,2).transpose()*pow(dt2,2);
  Aeq.block(2,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,2).transpose()*pow(dt1,2); 

   if (N_constraint == 4){    
      // 3rd order
      Aeq.block(3,0,1,poly_order+1) = t_vec(poly_order,1,3).transpose()*pow(dt2,3);
      Aeq.block(3,poly_order+1,1,poly_order+1) = -t_vec(poly_order,0,3).transpose()*pow(dt1,3); 
  }

  Constraint constraint;
  constraint.A = Aeq;
  constraint.b = beq;
  
  return constraint;
}

// output size 3x(2(N+1))
Constraint PathPlanner::get_continuity_constraint_mat3(double dt1,double dt2, TrajGenOpts opt)
{
  int N_constraint = 3*3;
  int poly_order = opt.poly_order;
  int blck_size = poly_order + 1;
  int blck_size_seg = blck_size * 3;
  Eigen::MatrixXd Aeq(N_constraint,3*2*(poly_order+1));
  Eigen::MatrixXd beq(N_constraint,1);   
  Aeq.setZero();
  beq.setZero();
  Eigen::MatrixXd D1 = time_scaling_mat(dt1,poly_order);
  Eigen::MatrixXd D2 = time_scaling_mat(dt2,poly_order);


  int insert_row,insert_col1,insert_col2; 
  
  for (int i = 0; i<3 ;i++){
      
      insert_row = 3*i; insert_col1 = blck_size*i,insert_col2 = blck_size_seg + insert_col1;
      
      // 0th order
      Aeq.block(insert_row,insert_col1,1,blck_size) = t_vec(poly_order,1,0).transpose();
      Aeq.block(insert_row,insert_col2,1,blck_size) = -t_vec(poly_order,0,0).transpose();
      
      // 1th order
      Aeq.block(insert_row+1,insert_col1,1,blck_size) = t_vec(poly_order,1,1).transpose()*dt2;
      Aeq.block(insert_row+1,insert_col2,1,blck_size) = -t_vec(poly_order,0,1).transpose()*dt1;
      
       // 2nd order
      Aeq.block(insert_row+2,insert_col1,1,blck_size) = t_vec(poly_order,1,2).transpose()*pow(dt2,2);
      Aeq.block(insert_row+2,insert_col2,1,blck_size) = -t_vec(poly_order,0,2).transpose()*pow(dt1,2);
       
  }

  Constraint constraint;
  constraint.A = Aeq;
  constraint.b = beq;
  
  return constraint;
}


/**
 * @brief 
 * 
 * @param pnt1 
 * @param pnt2 
 * @param t_vec 
 * @param option 
 * @return Constraint 
 */
Constraint PathPlanner::get_corridor_constraint_mat(Eigen::Vector3d p1, Eigen::Vector3d p2, VectorXd t_vec, TrajGenOpts option)
{
  const Eigen::Affine3d Twb = get_affine_corridor_pose(p1, p2);
  const int blck_size = t_vec.size(); // 3* blck_size  = blck_size_seg
  const double l = (p1 - p2).norm();

  Eigen::MatrixXd A_sub(3,3*blck_size);
  Eigen::Matrix3d Rbw = Twb.rotation().transpose();
  Eigen::Vector3d twb = Twb.translation();

  // matrix A_sub
  for (int r=0; r<3; r++) {
    for (int c=0; c<3; c++) {
      A_sub.block(r,c*blck_size,1,blck_size) = Rbw(r,c) * t_vec.transpose();
    }
  }

  Eigen::Vector3d upper_limit;
  upper_limit << l/2 + option.safe_r, option.safe_r, option.safe_r;
  Eigen::Vector3d lower_limit = -upper_limit;

  upper_limit += Rbw * twb;
  lower_limit += Rbw * twb;

  Constraint constraint;
  constraint.A = Eigen::MatrixXd(6,3*blck_size);
  constraint.A << A_sub, -A_sub;
  constraint.b = Eigen::MatrixXd(6,1);
  constraint.b << upper_limit, -lower_limit;
  
  return constraint;
}

Eigen::MatrixXd expand3(Eigen::MatrixXd small_mat)
{
  const int r = small_mat.rows();
  const int c = small_mat.cols();
  Eigen::MatrixXd new_mat = Eigen::MatrixXd::Zero(3*r,3*c);
  
  new_mat.block(0, 0, r, c) = small_mat;
  new_mat.block(r, c, r, c) = small_mat;
  new_mat.block(2*r ,2*c, r, c) = small_mat;
  return new_mat;
}

Eigen::MatrixXd blkdiag(Eigen::MatrixXd A1, Eigen::MatrixXd A2, Eigen::MatrixXd A3)
{
  const int r = A1.rows();
  const int c = A1.cols();
  Eigen::MatrixXd new_mat = Eigen::MatrixXd::Zero(3*r,3*c);
  new_mat.block(0,0,r,c) = A1;
  new_mat.block(r,c,r,c) = A2;
  new_mat.block(2*r,2*c,r,c) = A3;
  return new_mat;
}

Eigen::MatrixXd row_stack3(Eigen::MatrixXd mat1)
{
  Eigen::MatrixXd new_mat(3*mat1.rows(),mat1.cols());
  new_mat.block(0,0,mat1.rows(),mat1.cols()) = mat1;
  new_mat.block(mat1.rows(),0,mat1.rows(),mat1.cols()) = mat1;
  new_mat.block(2*mat1.rows(),0,mat1.rows(),mat1.cols()) = mat1;
  return new_mat;
}


Eigen::MatrixXd row_stack3(Eigen::MatrixXd mat1, Eigen::MatrixXd mat2, Eigen::MatrixXd mat3)
{
  Eigen::MatrixXd new_mat(3*mat1.rows(),mat1.cols());

  new_mat.block(0,0,mat1.rows(),mat1.cols()) = mat1;
  new_mat.block(mat1.rows(),0,mat1.rows(),mat1.cols()) = mat2;
  new_mat.block(2*mat1.rows(),0,mat1.rows(),mat1.cols()) = mat3;
  return new_mat;
}

/**
MatrixXd expand3(VectorXd small_vec){
    int len_small = small_vec.size();
    MatrixXd new_mat(1,3*len_small); new_mat.setZero();
    new_mat.block<0,0,1,len_small> = small_vec;
    new_mat.block<0,len_small,1,len_small> = small_vec;
    new_mat.block<0,2*len_small,1,len_small> = small_vec;
    return new_mat;
}
**/
Eigen::MatrixXd time_scaling_mat(double dt, int poly_order)
{
  const int n_coeffs = poly_order + 1;
  Eigen::MatrixXd D = Eigen::MatrixXd::Zero(n_coeffs, n_coeffs);
  for (int i=0; i<n_coeffs; i++) {
    D.coeffRef(i,i) = pow(dt,i);
  }
  return D;
}


Eigen::VectorXd t_vec(int poly_order, double t, int n_diff)
{
  Eigen::VectorXd vec(poly_order+1);
  vec.setZero();
  switch(n_diff){
    case 0:
      for(int i=0;i<poly_order+1;i++)
          vec.coeffRef(i)=pow(t,i);
      break;
    case 1:
      for(int i=1;i<poly_order+1;i++)
          vec.coeffRef(i)=i*pow(t,i-1);

      break;
    case 2:
      for(int i=2;i<poly_order+1;i++)
          vec.coeffRef(i)=i*(i-1)*pow(t,i-2);
      break;

    case 3:
      for(int i=3;i<poly_order+1;i++)
          vec.coeffRef(i)=i*(i-1)*(i-2)*pow(t,i-3);
      break;
  }

  return vec;
}


Eigen::MatrixXd integral_jerk_squared(int poly_order)
{
  // this is ingeral of jerk matrix from 0 to 1 given polynomial order
  const int n = poly_order;
  Eigen::MatrixXd Qj = Eigen::MatrixXd::Zero(n+1, n+1);

  for (int i=3; i<n+1; i++) {
    for (int j=3; j<n+1; j++) {
      if (i==3 && j==3) {
        Qj.coeffRef(i,j) = i*(i-1)*(i-2)*j*(j-1)*(j-2);
      } else {
        Qj.coeffRef(i,j) = i*(i-1)*(i-2)*j*(j-1)*(j-2) / (i+j-5);
      }
    }
  }

  return Qj;
}

Eigen::MatrixXd integral_snap_squared(int poly_order)
{
  // this is ingeral of jerk matrix from 0 to 1 given polynomial order
   int n=poly_order;
  Eigen::MatrixXd Qj(n+1,n+1);
  Qj.setZero();

  for(int i=4;i<n+1;i++)
      for(int j=4;j<n+1;j++)
          if(i==4 and j==4)
              Qj.coeffRef(i,j)=i*(i-1)*(i-2)*(i-3)*j*(j-1)*(j-2)*(j-3);
          else
              Qj.coeffRef(i,j)=i*(i-1)*(i-2)*(i-3)*j*(j-1)*(j-2)*(j-3)/(i+j-7);


  return Qj;
}

void row_append(Eigen::MatrixXd& mat, Eigen::MatrixXd mat_sub)
{
  int orig_row_size = mat.rows();
  mat.conservativeResize(mat.rows()+mat_sub.rows(),mat.cols());
  mat.block(orig_row_size,0,mat_sub.rows(),mat.cols()) = mat_sub;
}

int find_spline_interval(const std::vector<double>& ts, double t_eval)
{
  int idx=-1;
  
  for(int i=0;i<ts.size()-1;i++)
      if(ts[i]<=t_eval && ts[i+1]>t_eval)
          idx=i;
  if (t_eval >= ts.back())
      idx = ts.size()-2;

  return idx;

  // if idx == -1, then could not find
}

} // ns polytraj
} // ns acl
