Polynomial Trajectory Generation
================================

This ROS package provides functionality for polynomial trajectory optimization.

Also provides [`traj_gen`](https://github.com/icsl-Jeon/traj_gen).