/**
 * @file demo.cpp
 * @brief ROS demo node for polynomial trajectory optimization with snap stack
 * @author Parker Lusk <plusk@mit.edu>
 * @date 31 January 2021
 */

#include <tf2/utils.h>

#include "polytraj_ros/demo.h"

namespace acl {
namespace polytraj_ros {

double clamp(double d, double min, double max)
{
  const double t = d < min ? min : d;
  return t > max ? max : t;
}

// ----------------------------------------------------------------------------

Demo::Demo(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
: nh_(nh), nhp_(nhp)
{

  //
  // Load Parameters
  //

  // room bounds
  nh_.param<double>("/room_bounds/x_min", bounds_x_min_, 0.0);
  nh_.param<double>("/room_bounds/x_max", bounds_x_max_, 1.0);
  nh_.param<double>("/room_bounds/y_min", bounds_y_min_, 0.0);
  nh_.param<double>("/room_bounds/y_max", bounds_y_max_, 1.0);
  nh_.param<double>("/room_bounds/z_min", bounds_z_min_, 0.0);
  nh_.param<double>("/room_bounds/z_max", bounds_z_max_, 1.0);

  // general takeoff and landing params
  nh_.param<double>("cntrl/spinup_time", spinup_time_, 0.5);
  nhp_.param<double>("control_dt", control_dt_, 0.01);
  nhp_.param<double>("takeoff_inc", takeoff_inc_, 0.0035);
  nhp_.param<double>("takeoff_alt", takeoff_alt_, 1.0);
  nhp_.param<bool>("takeoff_rel", takeoff_rel_, false);
  nhp_.param<double>("landing_fast_threshold", landing_fast_threshold_, 0.400);
  nhp_.param<double>("landing_fast_dec", landing_fast_dec_, 0.0035);
  nhp_.param<double>("landing_slow_dec", landing_slow_dec_, 0.001);

  //
  // Visualization initialization
  //

  // Initialize setpoint marker
  vizsetpoint_.id = 0;
  vizsetpoint_.type = visualization_msgs::Marker::SPHERE;
  vizsetpoint_.pose.orientation.x = 0.0;
  vizsetpoint_.pose.orientation.y = 0.0;
  vizsetpoint_.pose.orientation.z = 0.0;
  vizsetpoint_.pose.orientation.w = 1.0;
  vizsetpoint_.scale.x = 0.35;
  vizsetpoint_.scale.y = 0.35;
  vizsetpoint_.scale.z = 0.35;
  vizsetpoint_.color.a = 0.7; // Don't forget to set the alpha!
  vizsetpoint_.color.r = 1.0;
  vizsetpoint_.color.g = 0.5;
  vizsetpoint_.color.b = 0.0;

  //
  // ROS pub / sub communication
  //

  pub_cmdout_ = nh_.advertise<snapstack_msgs::Goal>("goal", 1);
  pub_trajectory_ = nh_.advertise<nav_msgs::Path>("trajectory", 1);
  pubviz_waypoints_ = nh_.advertise<visualization_msgs::MarkerArray>("viz_waypoints", 1);
  pubviz_corridors_ = nh_.advertise<visualization_msgs::MarkerArray>("viz_corridors", 1);

  sub_waypoints_ = nh_.subscribe("waypoints", 1, &Demo::waypointsCb, this);
  sub_fmode_ = nh_.subscribe("/globalflightmode", 1, &Demo::flightmodeCb, this);
  sub_state_ = nh_.subscribe("state", 1, &Demo::stateCb, this);

  //
  // Timers
  //

  tim_control_ = nh_.createTimer(ros::Duration(control_dt_),
                                                &Demo::controlCb, this);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Demo::flightmodeCb(const snapstack_msgs::QuadFlightModeConstPtr& msg)
{
  // handle safety state machine transitions based on
  // global flight modes dispatched by the operator.

  if (mode_ == Mode::NOT_FLYING &&
        msg->mode == snapstack_msgs::QuadFlightMode::GO) {
    mode_ = Mode::TAKEOFF;
    ROS_INFO("Spinning up motors for takeoff...");

  } else if ((mode_ == Mode::TAKEOFF || mode_ == Mode::FLYING) &&
        msg->mode == snapstack_msgs::QuadFlightMode::LAND) {
    mode_ = Mode::LANDING;
    ROS_INFO("Landing...");

  } else if (msg->mode == snapstack_msgs::QuadFlightMode::KILL) {
    mode_ = Mode::NOT_FLYING;
    ROS_WARN("Killing!");

  }
}

// ----------------------------------------------------------------------------

void Demo::waypointsCb(const nav_msgs::PathConstPtr& msg)
{

  std::vector<Eigen::Vector3d> waypoints;
  waypoints.reserve(msg->poses.size()+1);

  // first add current position as a waypoint
  Eigen::Vector3d current_pos;
  current_pos.x() = pose_.pose.position.x;
  current_pos.y() = pose_.pose.position.y;
  current_pos.z() = pose_.pose.position.z;
  waypoints.push_back(current_pos);

  for (size_t i=0; i<msg->poses.size(); i++) {
    // make sure the desired waypoint is not out of bounds
    snapstack_msgs::Goal tmp;
    tmp.p.x = msg->poses[i].pose.position.x;
    tmp.p.y = msg->poses[i].pose.position.y;
    tmp.p.z = msg->poses[i].pose.position.z;
    makeGoalSafe(tmp);

    waypoints.push_back(Eigen::Vector3d(tmp.p.x, tmp.p.y, tmp.p.z));
  }

  traj_ = std::make_shared<polytraj::Trajectory>(polytraj_.fromWaypoints(waypoints));

  ROS_INFO_STREAM("Generated: " << *traj_);

  // publish trajectory as a path for visualization
  nav_msgs::Path path = sample_trajectory_into_path(*traj_, control_dt_, msg->header.frame_id);
  path.header = msg->header;
  pub_trajectory_.publish(path);
  // straight-line path and waypoints & safety corridor
  visualization_msgs::MarkerArray msgwps = 
                      create_waypoint_path_markers(*traj_, msg->header.frame_id);
  visualization_msgs::MarkerArray msgcor =
                      create_safety_corridor_markers(*traj_, msg->header.frame_id);

  pubviz_waypoints_.publish(msgwps);
  pubviz_corridors_.publish(msgcor);

  // indicate there is a new trajectory to follow
  replanned_ = true;
}

// ----------------------------------------------------------------------------

void Demo::stateCb(const snapstack_msgs::StateConstPtr& msg)
{
  pose_.header = msg->header;
  pose_.pose.position = msg->pos;
  pose_.pose.orientation = msg->quat;

  twist_.header = msg->header;
  twist_.twist.linear = msg->vel;
  twist_.twist.angular = msg->w;
}

// ----------------------------------------------------------------------------

void Demo::controlCb(const ros::TimerEvent& e)
{
  static snapstack_msgs::Goal goalmsg;
  static bool flight_initialized = false;
  static ros::Time takeoff_time;
  static double last_active_goal_time;
  static double takeoff_alt;
  static double initial_alt;

  if (mode_ == Mode::TAKEOFF) {

    if (!flight_initialized) {
      // capture the initial time
      takeoff_time = ros::Time::now();

      // set the goal to our current position + yaw
      goalmsg.p = pose_.pose.position;
      goalmsg.v.x = 0;
      goalmsg.v.y = 0;
      goalmsg.v.z = 0;
      goalmsg.psi = tf2::getYaw(pose_.pose.orientation);
      goalmsg.dpsi = 0;

      // There is no real velocity control, since the ACL outer loop tracks
      // trajectories and their derivatives. To achieve velocity control,
      // we will integrate the velocity commands to obtain the trajectory.
      goalmsg.mode_xy = snapstack_msgs::Goal::MODE_POSITION_CONTROL;
      goalmsg.mode_z = snapstack_msgs::Goal::MODE_POSITION_CONTROL;

      // allow the outer loop to send low-level autopilot commands
      goalmsg.power = true;

      // what is our initial altitude before takeoff?
      initial_alt = pose_.pose.position.z;

      // what should our desired takeoff altitude be?
      takeoff_alt = takeoff_alt_ + ((takeoff_rel_) ? initial_alt : 0.0);

      flight_initialized = true;
    }

    // wait for the motors to spin up all the way before sending a command
    if (ros::Time::now() - takeoff_time >= ros::Duration(spinup_time_)) {

      constexpr double TAKEOFF_THRESHOLD = 0.100;
      if ((std::abs(goalmsg.p.z - pose_.pose.position.z) < TAKEOFF_THRESHOLD) &&
            std::abs(goalmsg.p.z - takeoff_alt) < TAKEOFF_THRESHOLD) {
        mode_ = Mode::FLYING;
        ROS_INFO("Takeoff complete!");

        // reset the trajectory
        traj_.reset();
      } else {
        // Increment the z cmd each timestep for a smooth takeoff.
        // This is essentially saturating tracking error so actuation is low.
        goalmsg.p.z = clamp(goalmsg.p.z + takeoff_inc_, 0.0, takeoff_alt);
      }
    }

  } else if (mode_ == Mode::FLYING) {

    // start from beginning of trajectory if we got a new plan
    if (replanned_) {
      traj_start_t_ = ros::Time::now();
      replanned_ = false;
    }

    // if an optimized trajectory exists
    if (traj_) {
      const double t = (ros::Time::now() - traj_start_t_).toSec();
      Eigen::Matrix3d X = traj_->eval(t);

      goalmsg.p.x = X.col(0).x();
      goalmsg.p.y = X.col(0).y();
      goalmsg.p.z = X.col(0).z();
      goalmsg.v.x = X.col(1).x();
      goalmsg.v.y = X.col(1).y();
      goalmsg.v.z = X.col(1).z();
      goalmsg.a.x = X.col(2).x();
      goalmsg.a.y = X.col(2).y();
      goalmsg.a.z = X.col(2).z();
    }

    // make sure we don't hit any walls
    makeGoalSafe(goalmsg);

    // // Visualize current setpoint goal of trajectory
    // vizsetpoint_.header.stamp = ros::Time::now();
    // vizsetpoint_.header.frame_id = pose_.header.frame_id;
    // vizsetpoint_.pose.position.x = goalmsg.p.x;
    // vizsetpoint_.pose.position.y = goalmsg.p.y;
    // vizsetpoint_.pose.position.z = goalmsg.p.z;
    // pub_vizsetpoint_.publish(vizsetpoint_);

  } else if (mode_ == Mode::LANDING) {

    goalmsg.v.x = goalmsg.v.y = goalmsg.v.z = 0;
    goalmsg.dpsi = 0;

    // choose between fast landing and slow landing
    const double thr = landing_fast_threshold_ + ((takeoff_rel_) ?
                                    initial_alt : 0.0);
    const double dec = (pose_.pose.position.z > thr) ?
                                    landing_fast_dec_ : landing_slow_dec_;

    goalmsg.p.z = clamp(goalmsg.p.z - dec, -0.1, bounds_z_max_);

    // TODO: Make this closed loop (maybe vel based?)
    if (goalmsg.p.z == -0.1) {
      mode_ = Mode::NOT_FLYING;
      ROS_INFO("Landing complete!");
    }

  } else if (mode_ == Mode::NOT_FLYING) {
    goalmsg.power = false;
    flight_initialized = false;
  }

  goalmsg.header.stamp = ros::Time::now();
  goalmsg.header.frame_id = "body";
  pub_cmdout_.publish(goalmsg);
}

// ----------------------------------------------------------------------------

void Demo::makeGoalSafe(snapstack_msgs::Goal& goalmsg) const
{
  // n.b.: this isn't particularly intelligent. It just instantaneously stops
  // the vehicle (i.e., infinite acceleration change). However, for
  // trajectories that barely stray outside of the bounds, this should be
  // sufficient. Besides, out of bounds waypoints should not be allowed.

  bool unsafe_x = goalmsg.p.x<bounds_x_min_ || goalmsg.p.x>bounds_x_max_;
  bool unsafe_y = goalmsg.p.y<bounds_y_min_ || goalmsg.p.y>bounds_y_max_;
  bool unsafe_z = goalmsg.p.z<bounds_z_min_ || goalmsg.p.z>bounds_z_max_;

  // clamp position of trajectory to inside edge of room
  goalmsg.p.x = clamp(goalmsg.p.x, bounds_x_min_, bounds_x_max_);
  goalmsg.p.y = clamp(goalmsg.p.y, bounds_y_min_, bounds_y_max_);
  goalmsg.p.z = clamp(goalmsg.p.z, bounds_z_min_, bounds_z_max_);

  // get rid of higher-order motion in unsafe directions
  if (unsafe_x) goalmsg.v.x = goalmsg.a.x = goalmsg.j.x = 0;
  if (unsafe_y) goalmsg.v.y = goalmsg.a.y = goalmsg.j.y = 0;
  if (unsafe_z) goalmsg.v.z = goalmsg.a.z = goalmsg.j.z = 0;
}

} // ns polytraj_ros
} // ns acl
