/**
 * @file polytraj_ros.cpp
 * @brief Utilities, serializers, visualization stuff
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 March 2021
 */

#include <Eigen/Geometry>

#include "polytraj_ros/polytraj_ros.h"

namespace acl {
namespace polytraj_ros {

polytraj_msgs::Trajectory serialize_trajectory(const polytraj::Trajectory& traj)
{
  // return an empty msg if not valid
  if (!traj.spline_xyz.is_valid) return {};

  const size_t num_segments = traj.spline_xyz.n_seg;

  polytraj_msgs::Trajectory trajmsg;
  trajmsg.segments.resize(num_segments, {});
  trajmsg.waytimes.resize(num_segments, 0.0);
  trajmsg.waypoints.resize(num_segments, {});

  // we consider a segment to be the polynomial segment + the endpoint
  for (size_t i=0; i<num_segments; i++) {
    // coefficients of each polynomial segment
    trajmsg.segments[i].x.coeffs = traj.spline_xyz.spline_x.poly_coeff[i].coeff;
    trajmsg.segments[i].y.coeffs = traj.spline_xyz.spline_y.poly_coeff[i].coeff;
    trajmsg.segments[i].z.coeffs = traj.spline_xyz.spline_z.poly_coeff[i].coeff;

    // time at each knot point (excluding t0 == 0)
    trajmsg.waytimes[i] = traj.qt[i+1];

    // position at each knot point (excluding wp0 == start)
    // (TODO: use eigen_conversions tf::pointEigenToMsg)
    trajmsg.waypoints[i].x = traj.q[i+1].x();
    trajmsg.waypoints[i].y = traj.q[i+1].y();
    trajmsg.waypoints[i].z = traj.q[i+1].z();
  }

  // starting point (TODO: use eigen_conversions tf::pointEigenToMsg)
  trajmsg.startpoint.x = traj.q.front().x();
  trajmsg.startpoint.y = traj.q.front().y();
  trajmsg.startpoint.z = traj.q.front().z();

  // options used to create trajectory
  trajmsg.opts.poly_order = traj.opts.poly_order;
  trajmsg.opts.objective_derivative = traj.opts.objective_derivative;
  trajmsg.opts.soft_constrained_waypoints = traj.opts.is_waypoint_soft;
  trajmsg.opts.w_d = traj.opts.w_d;
  trajmsg.opts.corridor = traj.opts.is_single_corridor/* || traj.opts.is_multi_corridor*/;
  trajmsg.opts.safe_r = traj.opts.safe_r;
  trajmsg.opts.N_safe_pnts = traj.opts.N_safe_pnts;

  return trajmsg;
}

// ----------------------------------------------------------------------------

polytraj::Trajectory deserialize_trajectory(const polytraj_msgs::Trajectory& trajmsg)
{
  // return empty traj if not valid
  if (trajmsg.segments.size() == 0) return {};

  polytraj::Trajectory traj;
  traj.valid = true;

  // append waypoints, starting with startpoint
  traj.q.reserve(1 + trajmsg.waypoints.size());
  traj.q.push_back({});
  traj.q.back().x() = trajmsg.startpoint.x;
  traj.q.back().y() = trajmsg.startpoint.y;
  traj.q.back().z() = trajmsg.startpoint.z;
  for (const auto& pt : trajmsg.waypoints) {
    traj.q.push_back({});
    traj.q.back().x() = pt.x;
    traj.q.back().y() = pt.y;
    traj.q.back().z() = pt.z;
  }

  // append waypoint times, starting with 0
  traj.qt.reserve(1 + trajmsg.waytimes.size());
  traj.qt.push_back(0.0);
  traj.qt.insert(traj.qt.end(), trajmsg.waytimes.begin(), trajmsg.waytimes.end());

  // total trajectory time
  traj.tf = traj.qt.back();

  // spline
  const size_t num_segments = trajmsg.segments.size();
  const size_t poly_order = trajmsg.segments.front().x.coeffs.size() - 1;
  traj.spline_xyz.is_valid = true;
  traj.spline_xyz.n_seg = num_segments;
  traj.spline_xyz.poly_order = poly_order;
  traj.spline_xyz.knot_time = traj.qt;

  traj.spline_xyz.spline_x.is_valid = true;
  traj.spline_xyz.spline_x.n_seg = num_segments;
  traj.spline_xyz.spline_x.knot_time = traj.qt;

  traj.spline_xyz.spline_y.is_valid = true;
  traj.spline_xyz.spline_y.n_seg = num_segments;
  traj.spline_xyz.spline_y.knot_time = traj.qt;

  traj.spline_xyz.spline_z.is_valid = true;
  traj.spline_xyz.spline_z.n_seg = num_segments;
  traj.spline_xyz.spline_z.knot_time = traj.qt;

  for (size_t i=0; i<num_segments; i++) {
    traj.spline_xyz.spline_x.poly_coeff.push_back({});
    traj.spline_xyz.spline_x.poly_coeff.back().poly_order = poly_order;
    traj.spline_xyz.spline_x.poly_coeff.back().coeff = trajmsg.segments[i].x.coeffs;

    traj.spline_xyz.spline_y.poly_coeff.push_back({});
    traj.spline_xyz.spline_y.poly_coeff.back().poly_order = poly_order;
    traj.spline_xyz.spline_y.poly_coeff.back().coeff = trajmsg.segments[i].y.coeffs;

    traj.spline_xyz.spline_z.poly_coeff.push_back({});
    traj.spline_xyz.spline_z.poly_coeff.back().poly_order = poly_order;
    traj.spline_xyz.spline_z.poly_coeff.back().coeff = trajmsg.segments[i].z.coeffs;
  }

  // options
  traj.opts.poly_order = trajmsg.opts.poly_order;
  traj.opts.objective_derivative = trajmsg.opts.objective_derivative;
  traj.opts.is_waypoint_soft = trajmsg.opts.soft_constrained_waypoints;
  traj.opts.w_d = trajmsg.opts.w_d;
  traj.opts.is_single_corridor = trajmsg.opts.corridor;
  traj.opts.safe_r = trajmsg.opts.safe_r;
  traj.opts.N_safe_pnts = trajmsg.opts.N_safe_pnts;

  return traj;
}

// ----------------------------------------------------------------------------

nav_msgs::Path sample_trajectory_into_path(const polytraj::Trajectory& traj,
                                      double dt, const std::string& frame_id)
{
  // publish trajectory as a path for visualization
  nav_msgs::Path path;
  path.header.stamp = ros::Time::now();
  path.header.frame_id = frame_id;
  const int N = traj.tf / dt;
  for (size_t i=0; i<N; i++) {
    // sample the trajectory at the appropriate time
    const double t = i * dt;
    Eigen::Matrix3d X = traj.eval(t);

    // add its PoseStamped representation to the path
    geometry_msgs::PoseStamped posemsg;
    posemsg.header.stamp = ros::Time(t);
    posemsg.header.frame_id = frame_id;
    posemsg.pose.position.x = X.col(0).x();
    posemsg.pose.position.y = X.col(0).y();
    posemsg.pose.position.z = X.col(0).z();
    posemsg.pose.orientation.w = 1;
    path.poses.push_back(posemsg);
  }
  
  return path;
}

// ----------------------------------------------------------------------------

visualization_msgs::MarkerArray create_waypoint_path_markers(
                const polytraj::Trajectory& traj, const std::string& frame_id)
{
  visualization_msgs::MarkerArray msgwps;
  for (size_t i=0; i<traj.q.size(); i++) {

    visualization_msgs::Marker wp;
    wp.header.stamp = ros::Time::now();
    wp.header.frame_id = frame_id;
    wp.id = msgwps.markers.size();
    wp.action = visualization_msgs::Marker::ADD;
    wp.type = visualization_msgs::Marker::SPHERE;
    wp.pose.position.x = traj.q[i].x();
    wp.pose.position.y = traj.q[i].y();
    wp.pose.position.z = traj.q[i].z();
    wp.pose.orientation.w = 1.0;
    wp.scale.x = 0.25;
    wp.scale.y = 0.25;
    wp.scale.z = 0.25;
    wp.color.a = 1;
    wp.color.r = 0;
    wp.color.g = 0.447;
    wp.color.b = 0.741;
    msgwps.markers.push_back(wp);

    if (i > 0) {
      visualization_msgs::Marker line;
      line.header = wp.header;
      line.id = msgwps.markers.size();
      line.action = visualization_msgs::Marker::ADD;
      line.type = visualization_msgs::Marker::LINE_STRIP;
      line.pose.orientation.w = 1.0;
      line.scale.x = 0.05;
      line.color = wp.color;
      line.points.resize(2);
      line.points[0].x = traj.q[i-1].x();
      line.points[0].y = traj.q[i-1].y();
      line.points[0].z = traj.q[i-1].z();
      line.points[1].x = traj.q[i].x();
      line.points[1].y = traj.q[i].y();
      line.points[1].z = traj.q[i].z();
      msgwps.markers.push_back(line);
    }
  }

  return msgwps;
}

// ----------------------------------------------------------------------------

visualization_msgs::MarkerArray create_safety_corridor_markers(
                const polytraj::Trajectory& traj, const std::string& frame_id)
{
  visualization_msgs::MarkerArray msgcor;
  for (size_t i=1; i<traj.q.size(); i++) {
    Eigen::Affine3d Twb;
    Eigen::Vector3d s;
    std::tie(Twb, s) = traj.getSafeCorridor(i-1);
    Eigen::Quaterniond q(Twb.rotation());


    visualization_msgs::Marker corridor;
    corridor.header.stamp = ros::Time::now();
    corridor.header.frame_id = frame_id;
    corridor.id = msgcor.markers.size();
    corridor.action = visualization_msgs::Marker::ADD;
    corridor.type = visualization_msgs::Marker::CUBE;
    corridor.pose.orientation.x = q.x();
    corridor.pose.orientation.y = q.y();
    corridor.pose.orientation.z = q.z();
    corridor.pose.orientation.w = q.w();
    corridor.pose.position.x = Twb.translation().x();
    corridor.pose.position.y = Twb.translation().y();
    corridor.pose.position.z = Twb.translation().z();
    corridor.scale.x = s.x();
    corridor.scale.y = s.y();
    corridor.scale.z = s.z();
    corridor.color.a = 0.5;
    corridor.color.r = 170. / 255.;
    corridor.color.g = 1.;
    corridor.color.b = 1.;
    msgcor.markers.push_back(corridor);
  }

  return msgcor;
}

} // ns polytraj_ros
} // ns acl