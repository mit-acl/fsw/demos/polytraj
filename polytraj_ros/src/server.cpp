/**
 * @file server.cpp
 * @brief ROS server for polynomial trajectory generation
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 March 2021
 */

#include <sstream>

#include "polytraj_ros/server.h"

namespace acl {
namespace polytraj_ros {

Server::Server(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
: nh_(nh), nhp_(nhp)
{

  //
  // Load Parameters
  //

  // room bounds
  nh_.param<double>("/room_bounds/x_min", bounds_x_min_, 0.0);
  nh_.param<double>("/room_bounds/x_max", bounds_x_max_, 1.0);
  nh_.param<double>("/room_bounds/y_min", bounds_y_min_, 0.0);
  nh_.param<double>("/room_bounds/y_max", bounds_y_max_, 1.0);
  nh_.param<double>("/room_bounds/z_min", bounds_z_min_, 0.0);
  nh_.param<double>("/room_bounds/z_max", bounds_z_max_, 1.0);

  //
  // ROS communications
  //

  traj_srv_ = nhp_.advertiseService("generate", &Server::traj_cb, this);

}

// ----------------------------------------------------------------------------
// ROS callbacks
// ----------------------------------------------------------------------------

bool Server::traj_cb(polytraj_msgs::GenerateTrajectory::Request& req,
                    polytraj_msgs::GenerateTrajectory::Response& res)
{

  std::vector<Eigen::Vector3d> waypoints;
  waypoints.reserve(req.waypoints.size()+1);

  // first add starting point as a waypoint
  Eigen::Vector3d startpoint;
  startpoint.x() = req.startpoint.x;
  startpoint.y() = req.startpoint.y;
  startpoint.z() = req.startpoint.z;
  waypoints.push_back(startpoint);

  // add other waypoints
  for (const auto& wp : req.waypoints) {
    waypoints.push_back(Eigen::Vector3d(wp.x, wp.y, wp.z));
  }

  // check for any invalid waypoints
  for (int i=0; i<waypoints.size(); i++) {
    if (!is_waypoint_valid(waypoints[i])) {
      std::ostringstream ss;
      ss << "Waypoint " << i << " is invalid: " << waypoints[i].transpose();
      res.msg = ss.str();
      res.success = false;
      return true;
    }
  }

  // generate a trajectory
  const polytraj::Trajectory traj = polytraj_.fromWaypoints(waypoints);

  if (traj.valid) {
    res.trajectory = serialize_trajectory(traj);
    res.trajectory.header.stamp = ros::Time::now();
    res.trajectory.header.frame_id = req.header.frame_id;
    std::ostringstream ss;
    ss << traj;
    res.msg = ss.str();
    res.success = true;
  } else {
    res.msg = "Could not solve for trajectory.";
    res.success = false;
  }

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

bool Server::is_waypoint_valid(const Eigen::Vector3d& wp) const
{
  bool invalid_x = wp.x()<bounds_x_min_ || wp.x()>bounds_x_max_;
  bool invalid_y = wp.y()<bounds_y_min_ || wp.y()>bounds_y_max_;
  bool invalid_z = wp.z()<bounds_z_min_ || wp.z()>bounds_z_max_;

  return !invalid_x && !invalid_y && !invalid_z;
}

} // ns polytraj_ros
} // ns acl
