/**
 * @file server_node.cpp
 * @brief Entry point for polytraj ROS server
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 March 2021
 */

#include <ros/ros.h>

#include "polytraj_ros/server.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "polytraj_server");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::polytraj_ros::Server server(nhtopics, nhparams);
  ros::spin();
  return 0;
}