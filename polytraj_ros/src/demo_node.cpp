/**
 * @file demo_node.cpp
 * @brief Entry point for polytraj snap-stack demo ROS node
 * @author Parker Lusk <plusk@mit.edu>
 * @date 31 January 2021
 */

#include <ros/ros.h>

#include "polytraj_ros/demo.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "polytraj_demo");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::polytraj_ros::Demo demo(nhtopics, nhparams);
  ros::spin();
  return 0;
}