#!/usr/bin/env python
"""
Rviz Operator for Path Creation

This node enables an operator to send a path to the polynomial trajectory
optimizer using rviz goals. The altitude of the waypoints is set via rosparam.
"""

import rospy

from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseStamped, Point
from nav_msgs.msg import Path
from std_srvs.srv import Trigger, TriggerResponse


class Operator:

    def __init__(self):

        #
        # Load ROS Parameters
        # 

        # room bounds
        self.bounds = {}
        self.bounds['xmax'] = rospy.get_param('/room_bounds/x_max',  1.0)
        self.bounds['xmin'] = rospy.get_param('/room_bounds/x_min', -1.0)
        self.bounds['ymax'] = rospy.get_param('/room_bounds/y_max',  1.0)
        self.bounds['ymin'] = rospy.get_param('/room_bounds/y_min', -1.0)
        self.bounds['zmax'] = rospy.get_param('/room_bounds/z_max',  1.0)
        self.bounds['zmin'] = rospy.get_param('/room_bounds/z_min',  0.0)

        self.waypoint_z = rospy.get_param('~waypoint_z', 1.5)

        # the path currently being built
        self.path = Path()
        self.pathviz = MarkerArray()

        #
        # ROS pub / sub communication
        #

        self.pub_waypoints = rospy.Publisher("waypoints", Path, queue_size=1)
        self.pubviz_waypoints = rospy.Publisher("viz_waypoints", MarkerArray,
                                                queue_size=1, latch=True)

        self.sub_rvizgoal = rospy.Subscriber("~/move_base_simple/goal",
                                                PoseStamped, self.goalCB,
                                                queue_size=1)

        self.srv_commit = rospy.Service("~/commit", Trigger, self.commitCb)

    def goalCB(self, msg):
        # check that desired goal is within room bounds
        if (msg.pose.position.x < self.bounds['xmin']
                or msg.pose.position.x > self.bounds['xmax']
                or msg.pose.position.y < self.bounds['ymin']
                or msg.pose.position.y > self.bounds['ymax']):
            rospy.logerr('({:.2f}, {:.2f}) is out of bounds'.format(
                        msg.pose.position.x, msg.pose.position.y))
            return

        p = PoseStamped()
        p.header.stamp = rospy.Time.now()
        p.header.frame_id = "world"
        p.pose.position.x = msg.pose.position.x
        p.pose.position.y = msg.pose.position.y
        p.pose.position.z = self.waypoint_z
        p.pose.orientation.w = 1;
        self.path.poses.append(p)

        m = Marker()
        m.header = p.header
        m.id = len(self.pathviz.markers)
        m.type = Marker.SPHERE
        m.action = Marker.ADD
        m.color.a = 1
        m.color.r, m.color.g, m.color.b = (0, 0.4470, 0.7410)
        m.scale.x, m.scale.y, m.scale.z = 0.25, 0.25, 0.25
        m.pose = p.pose
        self.pathviz.markers.append(m)

        if len(self.path.poses) > 1:
            m = Marker()
            m.header = p.header
            m.id = len(self.pathviz.markers)
            m.type = Marker.LINE_STRIP
            m.action = Marker.ADD
            m.color.a = 1
            m.color.r, m.color.g, m.color.b = (0, 0.4470, 0.7410)
            m.scale.x = 0.05
            m.pose.orientation.w = 1
            pt1 = Point()
            pt1 = self.path.poses[-2].pose.position
            pt2 = Point()
            pt2 = self.path.poses[-1].pose.position
            m.points.append(pt1)
            m.points.append(pt2)
            self.pathviz.markers.append(m)

        self.pubviz_waypoints.publish(self.pathviz)


    def commitCb(self, req):

        # delete the current path visualization
        for m in self.pathviz.markers:
            m.action = Marker.DELETEALL
        self.pubviz_waypoints.publish(self.pathviz)

        # send the path to generate a trajectory
        self.path.header.stamp = rospy.Time.now()
        self.path.header.frame_id = "world"
        self.pub_waypoints.publish(self.path)

        # reset state for next path specification
        self.path = Path()
        self.pathviz = MarkerArray()
        return TriggerResponse(True, "Committed to path")


if __name__ == '__main__':

    rospy.init_node('demo_rviz_operator', anonymous=False)
    ns = rospy.get_namespace()
    try:
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.signal_shutdown("no namespace specified")
        else:
            print("Starting rviz path operator for: {}".format(ns))
            node = Operator()
            rospy.spin()
    except rospy.ROSInterruptException:
        pass
