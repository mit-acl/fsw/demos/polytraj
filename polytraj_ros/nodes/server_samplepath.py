#!/usr/bin/env python
"""
Pre-specified Test Trajectory Generation

This node sends a pre-specified test traj to the polytraj server via service.
"""

import rospy

import numpy as np

from geometry_msgs.msg import Point
from trajpoly_msgs.srv import GenerateTrajectory, GenerateTrajectoryRequest


class SamplePath:

    def __init__(self):

        #
        # Test trajectory
        #

        waypoints = np.array([[-1, 1, 1],
                              [1.2, 1.5, 1],
                              [1.3, 1.85, 1],
                              [1.3, 1.85, 2.2],
                              [2.7, 2.7, 2.7]])
        
        trajmsg = self.call_service(waypoints)
        self.visualize_trajectory(trajmsg)

    def call_service(self, waypoints):
        srv = '/polytraj_server/generate'
        rospy.wait_for_service(srv)
        try:
            generate = rospy.ServiceProxy(srv, GenerateTrajectory)

            req = GenerateTrajectoryRequest()
            req.startpoint = Point(0., 0., 0.)
            for wp in waypoints:
                req.waypoints.append(Point(wp[0], wp[1], wp[2]))

            print(req)
            print
            print
            res = generate(req)
            print(res)

            return res.trajectory if res.success else None
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)


    def visualize_trajectory(self, trajmsg):
        pass

    def eval_trajectory(self, trajmsg, t):
        pass

    def select_poly(self, trajmsg, t):
        pass

    def create_tvec(self, trajmsg, t):
        pass



if __name__ == '__main__':

    rospy.init_node('server_samplepath', anonymous=False)
    node = SamplePath()
