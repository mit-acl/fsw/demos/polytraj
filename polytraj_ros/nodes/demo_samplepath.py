#!/usr/bin/env python
"""
Pre-specified Sample Path Generation

This node sends a pre-specified sample path to the polytraj generator.
"""

import rospy

import numpy as np

from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseStamped, Point
from nav_msgs.msg import Path


class SamplePath:

    def __init__(self):

        #
        # Load ROS Parameters
        #

        # room bounds
        self.bounds = {}
        self.bounds['xmax'] = rospy.get_param('/room_bounds/x_max',  1.0)
        self.bounds['xmin'] = rospy.get_param('/room_bounds/x_min', -1.0)
        self.bounds['ymax'] = rospy.get_param('/room_bounds/y_max',  1.0)
        self.bounds['ymin'] = rospy.get_param('/room_bounds/y_min', -1.0)
        self.bounds['zmax'] = rospy.get_param('/room_bounds/z_max',  1.0)
        self.bounds['zmin'] = rospy.get_param('/room_bounds/z_min',  0.0)

        # the path currently being built
        self.path = Path()
        self.pathviz = MarkerArray()

        #
        # ROS pub / sub communication
        #

        self.pub_waypoints = rospy.Publisher("waypoints", Path, queue_size=1)
        self.pubviz_waypoints = rospy.Publisher("viz_waypoints", MarkerArray,
                                                queue_size=1, latch=True)

        # wait for things to spin up
        rospy.sleep(0.5)

        #
        # Sample path
        #

        waypoints = np.array([[-1, 1, 1],
                              [1.2, 1.5, 1],
                              [1.3, 1.85, 1],
                              [1.3, 1.85, 2.2],
                              [2.7, 2.7, 2.7]])
        self.send_path(waypoints)

    def send_path(self, waypoints):

        # delete the current path visualization
        for m in self.pathviz.markers:
            m.action = Marker.DELETEALL
        self.pubviz_waypoints.publish(self.pathviz)
        # reset state for next path specification
        self.path = Path()
        self.pathviz = MarkerArray()

        for wp in waypoints:

            # check that desired goal is within room bounds
            if (wp[0] < self.bounds['xmin']
                    or wp[0] > self.bounds['xmax']
                    or wp[1] < self.bounds['ymin']
                    or wp[1] > self.bounds['ymax']
                    or wp[2] < self.bounds['zmin']
                    or wp[2] > self.bounds['zmax']):
                rospy.logfatal('({:.2f}, {:.2f}, {:.2f}) is out of bounds'.format(
                            wp[0], wp[1], wp[2]))
                rospy.signal_shutdown('OOB')
                return

            p = PoseStamped()
            p.header.stamp = rospy.Time.now()
            p.header.frame_id = "world"
            p.pose.position.x = wp[0]
            p.pose.position.y = wp[1]
            p.pose.position.z = wp[2]
            p.pose.orientation.w = 1;
            self.path.poses.append(p)

            m = Marker()
            m.header = p.header
            m.id = len(self.pathviz.markers)
            m.type = Marker.SPHERE
            m.action = Marker.ADD
            m.color.a = 1
            m.color.r, m.color.g, m.color.b = (0, 0.4470, 0.7410)
            m.scale.x, m.scale.y, m.scale.z = 0.25, 0.25, 0.25
            m.pose = p.pose
            self.pathviz.markers.append(m)

            if len(self.path.poses) > 1:
                m = Marker()
                m.header = p.header
                m.id = len(self.pathviz.markers)
                m.type = Marker.LINE_STRIP
                m.action = Marker.ADD
                m.color.a = 1
                m.color.r, m.color.g, m.color.b = (0, 0.4470, 0.7410)
                m.scale.x = 0.05
                m.pose.orientation.w = 1
                pt1 = Point()
                pt1 = self.path.poses[-2].pose.position
                pt2 = Point()
                pt2 = self.path.poses[-1].pose.position
                m.points.append(pt1)
                m.points.append(pt2)
                self.pathviz.markers.append(m)

            self.pubviz_waypoints.publish(self.pathviz)


        # send the path to generate a trajectory
        self.path.header.stamp = rospy.Time.now()
        self.path.header.frame_id = "world"
        self.pub_waypoints.publish(self.path)


if __name__ == '__main__':

    rospy.init_node('demo_samplepath', anonymous=False)
    ns = rospy.get_namespace()
    try:
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.signal_shutdown("no namespace specified")
        else:
            print("Sending a sample polytraj path for: " + ns)
            node = SamplePath()
            # rospy.spin()
    except rospy.ROSInterruptException:
        pass
