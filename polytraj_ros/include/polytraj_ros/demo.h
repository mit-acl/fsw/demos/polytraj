/**
 * @file demo.h
 * @brief ROS demo node for polynomial trajectory optimization with snap stack
 * @author Parker Lusk <plusk@mit.edu>
 * @date 31 January 2021
 */

#pragma once

#include <memory>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <snapstack_msgs/Goal.h>
#include <snapstack_msgs/State.h>
#include <snapstack_msgs/QuadFlightMode.h>

#include <polytraj/polytraj.h>
#include <polytraj_ros/polytraj_ros.h>

namespace acl {
namespace polytraj_ros {

  class Demo
  {
  public:
    Demo(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);
    ~Demo() = default;

  private:
    ros::NodeHandle nh_, nhp_;    
    ros::Publisher pub_cmdout_, pub_trajectory_, pubviz_waypoints_, pubviz_corridors_;
    ros::Subscriber sub_fmode_, sub_waypoints_, sub_state_;
    ros::Timer tim_control_;

    // polytraj trajectory generator
    polytraj::TrajectoryGenerator polytraj_;

    /// \brief Parameters
    double bounds_x_min_, bounds_x_max_; ///< safety bounds to
    double bounds_y_min_, bounds_y_max_; ///< keep the vehicle
    double bounds_z_min_, bounds_z_max_; ///< in the room.
    double control_dt_; ///< period at which outer loop commands are sent
    double spinup_time_; ///< how long to wait for motors to spin up
    double takeoff_inc_; ///< altitude increment used for smooth takeoff
    double takeoff_alt_; ///< desired takeoff altitude (maybe relative)
    bool takeoff_rel_; ///< should desired alt be relative to current alt?
    double landing_fast_threshold_; ///< above this alt, land "fast"
    double landing_fast_dec_; ///< use bigger decrements for "fast" landing
    double landing_slow_dec_; ///< use smaller decrements for "slow" landing

    /// \brief Internal state
    enum class Mode { NOT_FLYING, TAKEOFF, FLYING, LANDING };
    Mode mode_ = Mode::NOT_FLYING; ///< current mode derived from global flight mode
    geometry_msgs::PoseStamped pose_; ///< current pose of the vehicle
    geometry_msgs::TwistStamped twist_; ///< current twist of vehicle
    std::shared_ptr<polytraj::Trajectory> traj_; ///< current valid trajectory
    visualization_msgs::Marker vizsetpoint_; ///< viz of current setpoint
    bool replanned_; ///< whether or not a new plan has been made
    ros::Time traj_start_t_; ///< start walltime of trajectory

    void flightmodeCb(const snapstack_msgs::QuadFlightModeConstPtr& msg);
    void waypointsCb(const nav_msgs::PathConstPtr& msg);
    void stateCb(const snapstack_msgs::StateConstPtr& msg);
    void controlCb(const ros::TimerEvent& e);
    void makeGoalSafe(snapstack_msgs::Goal& goalmsg) const;
  };

} // ns polytraj_ros
} // ns acl
