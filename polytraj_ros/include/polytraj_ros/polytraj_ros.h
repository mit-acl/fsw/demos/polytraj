/**
 * @file polytraj_ros.h
 * @brief Utilities, serializers, visualization stuff
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 March 2021
 */

#pragma once

#include <ros/ros.h>

#include <polytraj/polytraj.h>
#include <polytraj_msgs/Trajectory.h>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/MarkerArray.h>

namespace acl {
namespace polytraj_ros {

  polytraj_msgs::Trajectory serialize_trajectory(const polytraj::Trajectory& traj);
  polytraj::Trajectory deserialize_trajectory(const polytraj_msgs::Trajectory& trajmsg);

  nav_msgs::Path sample_trajectory_into_path(const polytraj::Trajectory& traj,
                                        double dt, const std::string& frame_id = "world");
  visualization_msgs::MarkerArray create_waypoint_path_markers(
                  const polytraj::Trajectory& traj, const std::string& frame_id = "world");
  visualization_msgs::MarkerArray create_safety_corridor_markers(
                  const polytraj::Trajectory& traj, const std::string& frame_id = "world");

} // ns polytraj_ros
} // ns acl
