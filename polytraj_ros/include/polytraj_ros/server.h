/**
 * @file server.h
 * @brief ROS server for polynomial trajectory generation
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 March 2021
 */

#pragma once

#include <memory>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <polytraj_msgs/GenerateTrajectory.h>

#include <polytraj/polytraj.h>
#include <polytraj_ros/polytraj_ros.h>

namespace acl {
namespace polytraj_ros {

  class Server
  {
  public:
    Server(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);
    ~Server() = default;

  private:
    ros::NodeHandle nh_, nhp_;    
    ros::Publisher pub_cmdout_, pub_trajectory_, pubviz_waypoints_;
    ros::Subscriber sub_fmode_, sub_waypoints_, sub_state_;
    ros::ServiceServer traj_srv_;

    // polytraj trajectory generator
    polytraj::TrajectoryGenerator polytraj_;

    /// \brief Parameters
    double bounds_x_min_, bounds_x_max_; ///< safety bounds to
    double bounds_y_min_, bounds_y_max_; ///< keep the vehicle
    double bounds_z_min_, bounds_z_max_; ///< in the room.

    /// \brief ROS callbacks
    bool traj_cb(polytraj_msgs::GenerateTrajectory::Request& req,
                polytraj_msgs::GenerateTrajectory::Response& res);

    /// \brief Helper methods
    bool is_waypoint_valid(const Eigen::Vector3d& wp) const;
  };

} // ns polytraj_ros
} // ns acl
